﻿using System.Collections.Generic;
using BetWin.EfDAL.Repositories;
using BetWin.Model;

namespace BetWin.Business.BusinessRepositories
{
    public interface IGroupBusinessRepository
    {
        IEnumerable<Group> GetAllGroupsByTournament(int tournamentId);
        Group GetGroupById(int groupId);
        void AddGroup(params Group[] groups);
        void UpdateGroup(params Group[] groups);
        void RemoveGroup(params Group[] groups);
    }

    public class GroupBusinessRepository : IGroupBusinessRepository
    {
        private readonly IGroupRepository _repository;


        public GroupBusinessRepository()
        {
            _repository = new GroupRepository();
        }

        public GroupBusinessRepository(IGroupRepository repository)
        {
            _repository = repository;
        }


        public void AddGroup(params Group[] groups)
        {
            _repository.Add(groups);
        }

        public IEnumerable<Group> GetAllGroupsByTournament(int tournamentId)
        {
            return _repository.GetList(g => g.TournamentId == tournamentId,
                g => g.Teams,
                g => g.Matches);
        }

        public Group GetGroupById(int groupId)
        {
            return _repository.GetSingle(g => g.GroupId == groupId,
                g => g.Teams,
                g => g.Matches);
        }

        public void RemoveGroup(params Group[] groups)
        {
            _repository.Remove(groups);
        }

        public void UpdateGroup(params Group[] groups)
        {
            _repository.Update(groups);
        }
    }
}
