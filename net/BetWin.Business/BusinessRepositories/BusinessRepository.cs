﻿using BetWin.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using BetWin.EfDAL.Repositories;

namespace BetWin.Business.BusinessRepositories
{
    public interface IBusinessRepository
    {
        IEnumerable<Tournament> GetAllTournaments();
        Tournament GetTournamentById(int tournamentId);
        Tournament GetTournamentByCode(string code);
        void AddTournament(params Tournament[] tournaments);
        void UpdateTournament(params Tournament[] tournaments);
        void RemoveTournament(params Tournament[] tournaments);

        IEnumerable<Group> GetAllGroupsByTournament(int tournamentId);
        Group GetGroupById(int groupId);
        void AddGroup(params Group[] groups);
        void UpdateGroup(params Group[] groups);
        void RemoveGroup(params Group[] groups);

        IEnumerable<Match> GetAllMatchesByTournamentId(int tournamentId);
        IEnumerable<Match> GetAllMatchesByTournamentCode(string tournamentCode);
        IEnumerable<Match> GetAllMatchesByGroup(int groupId);
        Match GetMatchById(int matchId);
        void AddMatch(params Match[] matches);
        void UpdateMatch(params Match[] matches);
        void RemoveMatch(params Match[] matches);
    }

    public class BusinessRepository : IBusinessRepository
    {
        private readonly ITournamentRepository _tournamentRepository;
        private readonly IGroupRepository _groupRepository;
        private readonly IMatchRepository _matchRepository;

        public BusinessRepository()
        {
            _tournamentRepository = new TournamentRepository();
            _groupRepository = new GroupRepository();
            _matchRepository = new MatchRepository();
        }

        public BusinessRepository(ITournamentRepository tournamentRepository, 
            IGroupRepository groupRepository, IMatchRepository matchRepository)
        {
            _tournamentRepository = tournamentRepository;
            _groupRepository = groupRepository;
            _matchRepository = matchRepository;
        }


        public void AddTournament(params Tournament[] tournaments)
        {
            /* Validation and error handling omitted */
            _tournamentRepository.Add(tournaments);
        }

        public IEnumerable<Tournament> GetAllTournaments()
        {
            return _tournamentRepository.GetAll();
        }

        public Tournament GetTournamentById(int tournamentId)
        {
            return _tournamentRepository.GetSingle(t => t.TournamentId == tournamentId);
        }

        public Tournament GetTournamentByCode(string code)
        {
            return _tournamentRepository.GetSingle(t => t.Code.Equals(code, StringComparison.InvariantCultureIgnoreCase));
        }

        public void RemoveTournament(params Tournament[] tournaments)
        {
            /* Validation and error handling omitted */
            _tournamentRepository.Remove(tournaments);
        }

        public void UpdateTournament(params Tournament[] tournaments)
        {
            /* Validation and error handling omitted */
            _tournamentRepository.Update(tournaments);
        }
        

        public void AddGroup(params Group[] groups)
        {
            _groupRepository.Add(groups);
        }

        public IEnumerable<Group> GetAllGroupsByTournament(int tournamentId)
        {
            return _groupRepository.GetList(g => g.TournamentId == tournamentId,
                g => g.Teams,
                g => g.Matches);
        }

        public Group GetGroupById(int groupId)
        {
            return _groupRepository.GetSingle(g => g.GroupId == groupId,
                g => g.Teams,
                g => g.Matches);
        }

        public void RemoveGroup(params Group[] groups)
        {
            _groupRepository.Remove(groups);
        }

        public void UpdateGroup(params Group[] groups)
        {
            _groupRepository.Update(groups);
        }


        public void AddMatch(params Match[] matches)
        {
            /* Validation and error handling omitted */
            _matchRepository.Add(matches);
        }

        public IEnumerable<Match> GetAllMatchesByGroup(int groupId)
        {
            return _matchRepository.GetList(m => m.GroupId == groupId);
        }

        public IEnumerable<Match> GetAllMatchesByTournamentId(int tournamentId)
        {
            var qMatches =
                from match in _matchRepository.GetList(m => m.TournamentId == tournamentId)
                select match;

            return qMatches;
        }

        public IEnumerable<Match> GetAllMatchesByTournamentCode(string tournamentCode)
        {
            var qMatches =
                from tournamet in _tournamentRepository.GetAll()
                join match in _matchRepository.GetAll() on tournamet.TournamentId equals match.TournamentId
                where tournamet.Code.Equals(tournamentCode, StringComparison.CurrentCultureIgnoreCase)
                select match;
            
            return qMatches;
        }

        public Match GetMatchById(int matchId)
        {
            return _matchRepository.GetSingle(
                m => m.MatchId == matchId,
                m => m.FinalScore,
                m => m.Scores,
                m => m.TeamA,
                m => m.TeamB,
                m => m.Events);
        }

        public void RemoveMatch(params Match[] matches)
        {
            /* Validation and error handling omitted */
            _matchRepository.Remove(matches);
        }

        public void UpdateMatch(params Match[] matches)
        {
            /* Validation and error handling omitted */
            _matchRepository.Update(matches);
        }

        
    }
}
