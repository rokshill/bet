﻿using System;
using System.Collections.Generic;
using BetWin.EfDAL.Repositories;
using BetWin.Model;

namespace BetWin.Business.BusinessRepositories
{
    public interface ITournamentBusinessRepository
    {
        IEnumerable<Tournament> GetAllTournaments();
        Tournament GetTournamentById(int tournamentId);
        Tournament GetTournamentByCode(string code);
        void AddTournament(params Tournament[] tournaments);
        void UpdateTournament(params Tournament[] tournaments);
        void RemoveTournament(params Tournament[] tournaments);
    }

    public class TournamentBusinessRepository : ITournamentBusinessRepository
    {
        private readonly ITournamentRepository _repository;

        public TournamentBusinessRepository()
        {
            _repository = new TournamentRepository();
        }

        public TournamentBusinessRepository(ITournamentRepository repository)
        {
            _repository = repository;
        }


        public void AddTournament(params Tournament[] tournaments)
        {
            /* Validation and error handling omitted */
            _repository.Add(tournaments);
        }

        public IEnumerable<Tournament> GetAllTournaments()
        {
            return _repository.GetAll();
        }

        public Tournament GetTournamentById(int tournamentId)
        {
            return _repository.GetSingle(t => t.TournamentId == tournamentId);
        }

        public Tournament GetTournamentByCode(string code)
        {
            return _repository.GetSingle(t => t.Code.Equals(code, StringComparison.InvariantCultureIgnoreCase));
        }

        public void RemoveTournament(params Tournament[] tournaments)
        {
            /* Validation and error handling omitted */
            _repository.Remove(tournaments);
        }

        public void UpdateTournament(params Tournament[] tournaments)
        {
            /* Validation and error handling omitted */
            _repository.Update(tournaments);
        }
    }
}
