﻿using System.Collections.Generic;
using System.Linq;
using BetWin.EfDAL.Repositories;
using BetWin.Model;

namespace BetWin.Business.BusinessRepositories
{
    public interface IMatchBusinessRepository
    {
        IEnumerable<Match> GetAllMatchesByTournamentId(int tournamentId);
        IEnumerable<Match> GetAllMatchesByGroup(int groupId);

        Match GetMatchById(int matchId);
        void AddMatch(params Match[] matches);
        void UpdateMatch(params Match[] matches);
        void RemoveMatch(params Match[] matches);
    }

    public class MatchBusinessRepository : IMatchBusinessRepository
    {
        private readonly IMatchRepository _repository;

        public MatchBusinessRepository()
        {
            _repository = new MatchRepository();
        }

        public MatchBusinessRepository(IMatchRepository repository)
        {
            _repository = repository;
        }


        public void AddMatch(params Match[] matches)
        {
            /* Validation and error handling omitted */
            _repository.Add(matches);
        }

        public IEnumerable<Match> GetAllMatchesByGroup(int groupId)
        {
            return _repository.GetList(m => m.GroupId == groupId);
        }

        public IEnumerable<Match> GetAllMatchesByTournamentId(int tournamentId)
        {
            var qMatches =
                from match in _repository.GetList(m => m.TournamentId == tournamentId)
                select match;

            return qMatches;
        }

       
        public Match GetMatchById(int matchId)
        {
            return _repository.GetSingle(
                m => m.MatchId == matchId, 
                m => m.FinalScore, 
                m => m.Scores, 
                m => m.TeamA, 
                m => m.TeamB, 
                m => m.Events);
        }

        public void RemoveMatch(params Match[] matches)
        {
            /* Validation and error handling omitted */
            _repository.Remove(matches);
        }

        public void UpdateMatch(params Match[] matches)
        {
            /* Validation and error handling omitted */
            _repository.Update(matches);
        }
    }
}
