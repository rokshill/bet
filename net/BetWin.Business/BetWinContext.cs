﻿using BetWin.Business.BusinessRepositories;

namespace BetWin.Business
{
    // BetWin Context hosts all repositories
    public static class BetWinContext
    {
        public static ITournamentBusinessRepository TournamentRepo => new TournamentBusinessRepository();

        public static IGroupBusinessRepository GroupRepo => new GroupBusinessRepository();

        public static IMatchBusinessRepository MatchRepo => new MatchBusinessRepository();
    }
}
