﻿using BetWin.Model;
using System;

namespace BetWin.Business.Calculators
{
    public abstract class BetCalculator<TBet, TBetCalculatorConfig, TBetResult, TBetQuestion> ://, X> : 
        IBetCalculator<TBet, TBetCalculatorConfig, TBetResult, TBetQuestion> 
        where TBet : IBet
        where TBetCalculatorConfig : IBetCalculatorConfig
        where TBetResult : IBetResult
        where TBetQuestion : IBetQuestion
        //where X : class, IBetCalculator<T,U,V,W>
    {
        public TBetCalculatorConfig Config { get; set; }

        public BetCalculator(TBetCalculatorConfig config)
        {
            if (config == null)
                throw new ArgumentNullException(nameof(config));

            this.Config = config;
        }
        
        public abstract TBetResult Calculate(TBet bet, TBetQuestion betQuestion);
    }
}
