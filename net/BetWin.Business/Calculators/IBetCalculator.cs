﻿using BetWin.Model;

namespace BetWin.Business.Calculators
{
    public interface IBetCalculator<TBet, TBetCalculatorConfig, TBetResult, TBetQuestion> 
        where TBet : IBet
        where TBetCalculatorConfig : IBetCalculatorConfig
        where TBetResult : IBetResult
        where TBetQuestion : IBetQuestion
    {
        TBetCalculatorConfig Config { get; }

        TBetResult Calculate(TBet bet, TBetQuestion betQuestion);
    }
}
