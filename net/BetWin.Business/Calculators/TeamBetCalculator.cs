﻿using System;
using BetWin.Model;

namespace BetWin.Business.Calculators
{
    public interface ITeamBetCalculator :
        IBetCalculator<
            ITeamBet,
            ITeamCalculatorConfig,
            ITeamBetResult,
            ITeamQuestion>
    {
    }

    public class TeamBetCalculator :
        BetCalculator<
            ITeamBet,
            ITeamCalculatorConfig,
            ITeamBetResult,
            ITeamQuestion>,
            ITeamBetCalculator
    {
        public TeamBetCalculator(ITeamCalculatorConfig config)
            : base(config)
        {
        }

        public override ITeamBetResult Calculate(ITeamBet bet, ITeamQuestion betQuestion)
        {
            throw new NotImplementedException();
        }
    }
}
