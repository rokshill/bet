﻿using System;
using BetWin.Model;

namespace BetWin.Business.Calculators
{
    public interface IPlayerBetCalculator :
        IBetCalculator<
            IPlayerBet,
            IPlayerCalculatorConfig,
            IPlayerBetResult,
            IPlayerQuestion>
    {
    }

    public class PlayerBetCalculator :
        BetCalculator<
            IPlayerBet,
            IPlayerCalculatorConfig,
            IPlayerBetResult,
            IPlayerQuestion>,
            IPlayerBetCalculator

    {
        
        public PlayerBetCalculator(PlayerCalculatorConfig config)
            : base(config)
        {
        }

        public override IPlayerBetResult Calculate(IPlayerBet bet, IPlayerQuestion betQuestion)
        {
            throw new NotImplementedException();
        }
    }
}
