﻿using BetWin.Model;


namespace BetWin.Business.Calculators
{
    public interface IMatchScoreBetCalculator :
        IBetCalculator<
            IMatchScoreBet,
            IMatchScoreCalculatorConfig,
            IMatchScoreBetResult,
            IMatchScoreQuestion>
    {
    }

    public class MatchScoreBetCalculator : 
        BetCalculator<
            IMatchScoreBet,
            IMatchScoreCalculatorConfig,
            IMatchScoreBetResult,
            IMatchScoreQuestion>,       
        IMatchScoreBetCalculator
    {
       public MatchScoreBetCalculator(IMatchScoreCalculatorConfig config)
            : base(config)
        {
        }

        public override IMatchScoreBetResult Calculate(IMatchScoreBet bet, IMatchScoreQuestion betQuestion)
        {
            var result = new MatchScoreBetResult
            {
                Status = BetResultStatus.Calculated,
                Points = 0
            };

            if (bet.MatchScore.EqualsInExact(betQuestion.MatchScore))
            {
                result.Points = Config.ExactScorePoints;
            }
            else if (bet.MatchScore.EqualsInGeneral(betQuestion.MatchScore))
            {
                result.Points = Config.GeneralScorePoints;
            }

            return result;
        }
    }
}
