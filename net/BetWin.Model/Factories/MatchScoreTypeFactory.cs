﻿namespace BetWin.Model
{
    public class MatchScoreTypeFactory : IMatchScoreTypeFactory
    {
        public IMatchScoreBet CreateBet()
        {
            return new MatchScoreBet();
        }

        public IMatchScoreBet CreateBet(MatchScoreType type, int scoreA, int scoreB)
        {
            return new MatchScoreBet(type, scoreA, scoreB);
        }

        public IMatchScoreCalculatorConfig CreateBetCalculatorConfig()
        {
            return new MatchScoreCalculatorConfig();
        }

        public IMatchScoreCalculatorConfig CreateBetCalculatorConfig(int generalScorePoints, int exactScorePoints)
        {
            return new MatchScoreCalculatorConfig(generalScorePoints, exactScorePoints);
        }

        public IMatchScoreQuestion CreateBetQuestion()
        {
            return new MatchScoreQuestion();
        }

        public IMatchScoreQuestionResult CreateBetQuestionResult()
        {
            return new MatchScoreQuestion();
        }

        public IMatchScoreQuestionResult CreateBetQuestionResult(MatchScoreType scoreType, int scoreA, int scoreB)
        {
            return new MatchScoreQuestion(scoreType, scoreA, scoreB);
        }

        public IMatchScoreBetResult CreateBetResult()
        {
            return new MatchScoreBetResult();
        }

        

        public IMatchScore CreateMatchScore()
        {
            return new MatchScore();
        }

        public IMatchScore CreateMatchScore(MatchScoreType type, int scoreA, int scoreB)
        {
            return new MatchScore(type, scoreA, scoreB);
        }
    }
}
