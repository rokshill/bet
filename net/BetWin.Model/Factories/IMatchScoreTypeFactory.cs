﻿namespace BetWin.Model
{
    public interface IMatchScoreTypeFactory : 
        IGenericBetTypeFactory<
            IMatchScoreQuestion, 
            IMatchScoreBet, 
            IMatchScoreQuestionResult, 
            IMatchScoreBetResult, 
            IMatchScoreCalculatorConfig,
            IMatchScore>
    {
        IMatchScoreBet CreateBet(MatchScoreType type, int scoreA, int scoreB);

        IMatchScoreCalculatorConfig CreateBetCalculatorConfig(int generalScorePoints, int exactScorePoints);

        IMatchScoreQuestionResult CreateBetQuestionResult(MatchScoreType scoreType, int scoreA, int scoreB);
        

        IMatchScore CreateMatchScore();
        IMatchScore CreateMatchScore(MatchScoreType type, int scoreA, int scoreB);
    }
}
