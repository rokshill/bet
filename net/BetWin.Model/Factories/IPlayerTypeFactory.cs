﻿namespace BetWin.Model
{
    public interface IPlayerTypeFactory : 
        IGenericBetTypeFactory<
            IPlayerQuestion,
            IPlayerBet,
            IPlayerQuestionResult,
            IPlayerBetResult,
            IPlayerCalculatorConfig,
            IPlayer>
    {
    }
}
