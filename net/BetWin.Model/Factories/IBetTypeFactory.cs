﻿namespace BetWin.Model
{
    public interface IGenericBetTypeFactory<T, U, V, W, X, Y> 
        where T : IBetQuestion
        where U : IBet
        where V : IBetQuestionResult
        where W : IBetResult
        where X : IBetCalculatorConfig
        //where Y : IBetType
    {
        T CreateBetQuestion();
        U CreateBet();
        V CreateBetQuestionResult();
        W CreateBetResult();
        X CreateBetCalculatorConfig();
        //Y CreateType();
    }
}
