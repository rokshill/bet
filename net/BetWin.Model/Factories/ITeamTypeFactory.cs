﻿namespace BetWin.Model
{
    public interface ITeamTypeFactory :
        IGenericBetTypeFactory<
            ITeamQuestion,
            ITeamBet,
            ITeamQuestionResult,
            ITeamBetResult,
            ITeamCalculatorConfig,
            ITeam>
    {
    }
}
