﻿namespace BetWin.Model
{
    public enum MatchGeneralScoreType
    {
        IncorrectScore, 
        HomeScore, 
        DrawScore,
        AwayScore
    }
}
