﻿using System.ComponentModel.DataAnnotations;

namespace BetWin.Model
{
    public enum MatchType
    {
        [Display(Name = "Faza grupowa")]
        Group,
        [Display(Name = "Faza pucharowa")]
        Cup
    }
}
