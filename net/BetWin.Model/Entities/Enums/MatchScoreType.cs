﻿using System.ComponentModel.DataAnnotations;

namespace BetWin.Model
{
    public enum MatchScoreType
    {
        [Display(Name="Aktualny wynik")]
        Current,
        [Display(Name = "Wynik po pierwszej połowie")]
        HalfTime,
        [Display(Name = "Wynik po drugiej połowie")]
        RegularTime,
        [Display(Name = "Wynik po dogrywce")]
        ExtraTime,
        [Display(Name = "Wynik po karnych")]
        Penalties
    }
}
