﻿namespace BetWin.Model
{
    public enum BetType
    {
        MatchScore,
        Team,
        Player
    }
}
