﻿namespace BetWin.Model
{
    public enum MatchEventType
    {
        YellowCard,
        SecondYellowCard,
        RedCard,
        Goal,
        OwnGoal,
        PenaltyGoal
    }
}
