﻿namespace BetWin.Model
{
    public enum UserRole
    {
        Admin,
        Moderator,
        User
    }
}
