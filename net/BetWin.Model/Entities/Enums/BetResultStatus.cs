﻿namespace BetWin.Model
{
    public enum BetResultStatus
    {
        Calculated,
        Incorrect
    }
}
