﻿using System.ComponentModel.DataAnnotations;

namespace BetWin.Model
{
    public enum TournamentType
    {
        [Display(Name="Turniej")]
        Tournament,
        [Display(Name = "Liga")]
        League
    }
}
