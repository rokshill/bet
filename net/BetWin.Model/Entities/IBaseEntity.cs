﻿using System;

namespace BetWin.Model
{
    public interface IBaseEntity
    {
        int CreatedById { get; set; }
        DateTime CreatedOn { get; set; }
        int? ModifiedById { get; set; }
        DateTime? ModifiedOn { get; set; }

        User CreatedBy { get; set; }
        User ModifiedBy { get; set; }
    }
}
