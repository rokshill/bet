﻿using System.Collections.Generic;

namespace BetWin.Model
{
    public interface ITeam : IBaseEntity, ITeamQuestionResult, IBetType
    {
    }

    public partial class Team : BaseEntity, ITeam
    {
        public int TeamId { get; set; }

        public BetType Type { get; set; } = BetType.Team;
        public string Name { get; set; }
        public string ShortName { get; set; }
        public string CrestUrl { get; set; }

        public int? GroupId { get; set; }

        public virtual Group Group { get; set; }
        
        public virtual ICollection<Match> HomeMatches { get; set; } = new HashSet<Match>();
        public virtual ICollection<Match> AwayMatches { get; set; } = new HashSet<Match>();
        public virtual ICollection<MatchEvent> Events { get; set; } = new HashSet<MatchEvent>();
        public virtual ICollection<TournamentTeamPlayer> TournamentsPlayers { get; set; } = new HashSet<TournamentTeamPlayer>();
        public virtual ICollection<TeamBet> Bets { get; set; } = new HashSet<TeamBet>();
    }
}
