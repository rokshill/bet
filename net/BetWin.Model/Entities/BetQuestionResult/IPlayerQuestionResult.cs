﻿namespace BetWin.Model
{
    public interface IPlayerQuestionResult : IBetQuestionResult
    {
        int PlayerId { get; set; }

        //IPlayer Player { get; set; }
    }
}
