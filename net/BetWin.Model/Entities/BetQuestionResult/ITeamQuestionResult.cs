﻿namespace BetWin.Model
{
    public interface ITeamQuestionResult : IBetQuestionResult
    {
        int TeamId { get; set; }

        //ITeam Team { get; set; }
    }
}
