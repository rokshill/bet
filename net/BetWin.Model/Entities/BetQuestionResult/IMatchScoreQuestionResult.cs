﻿namespace BetWin.Model
{
    public interface IMatchScoreQuestionResultId : IBetQuestionResult
    {
        int MatchScoreId { get; set; }

        //IMatchScore MatchScore { get; set; }
    }

    public interface IMatchScoreQuestionResult : IMatchScoreQuestionResultId
    {
        MatchScore MatchScore { get; set; }
    }
}
