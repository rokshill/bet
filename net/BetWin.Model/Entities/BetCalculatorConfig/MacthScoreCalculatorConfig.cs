﻿namespace BetWin.Model
{
    public interface IMatchScoreCalculatorConfig : IBetCalculatorConfig
    {
        int GeneralScorePoints { get; set; }

        int ExactScorePoints { get; set; }
    }

    public class MatchScoreCalculatorConfig : BetCalculatorConfig, IMatchScoreCalculatorConfig 
    {
        public int GeneralScorePoints { get; set; }

        public int ExactScorePoints { get; set; }

        public MatchScoreCalculatorConfig()
        {
            this.Type = BetType.MatchScore;
        }

        public MatchScoreCalculatorConfig(int generalScorePoints, int exactScorePoints)
        {
            this.GeneralScorePoints = generalScorePoints;
            this.ExactScorePoints = exactScorePoints;
        }
    }
}
