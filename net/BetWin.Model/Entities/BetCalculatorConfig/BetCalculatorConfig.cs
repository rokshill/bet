﻿namespace BetWin.Model
{
    public class BetCalculatorConfig : BaseEntity, IBetCalculatorConfig
    {
        public int BetCalculatorConfigId { get; set; }

        public BetType Type { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
