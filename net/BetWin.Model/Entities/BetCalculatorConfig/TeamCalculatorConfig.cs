﻿namespace BetWin.Model
{
    public interface ITeamCalculatorConfig : IBetCalculatorConfig
    {
        int ExactTeamPoints { get; set; }
    }

    public class TeamCalculatorConfig : BetCalculatorConfig, ITeamCalculatorConfig
    {
        public int ExactTeamPoints { get; set; }
    }
}
