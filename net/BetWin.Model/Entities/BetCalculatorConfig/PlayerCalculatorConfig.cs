﻿namespace BetWin.Model
{
    public interface IPlayerCalculatorConfig : IBetCalculatorConfig
    {
        int ExactPlayerPoints { get; set; }
    }

    public class PlayerCalculatorConfig : BetCalculatorConfig, IPlayerCalculatorConfig
    {
        public int ExactPlayerPoints { get; set; }
    }
}
