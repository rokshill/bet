﻿namespace BetWin.Model
{
    public interface IBetCalculatorConfig : IBaseEntity
    {
        BetType Type { get; }
    }
}
