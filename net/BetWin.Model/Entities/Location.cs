﻿using System.Collections.Generic;

namespace BetWin.Model
{
    public class Location : BaseEntity
    {
        public int LocationId { get; set; }

        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public int Capacity { get; set; }
        
        public virtual ICollection<Match> Matches { get; set; } = new HashSet<Match>();
    }
}
