﻿namespace BetWin.Model
{
    public class MatchEvent : BaseEntity
    {
        public int MatchEventId { get; set; }

        public MatchEventType Type { get; set; }
        public int Minute { get; set; }

        public int? PlayerId { get; set; }
        public int TeamId { get; set; }
        public int MatchId { get; set; }

        public virtual Player Player { get; set; }
        public virtual Team Team { get; set; }
        public virtual Match Match { get; set; }
    }
}
