﻿namespace BetWin.Model
{
    public interface ITeamBet : IBet
    {
        int? TeamId { get; set; }

        ITeam BetTeam { get; set; }
    }

    public class TeamBet : Bet, ITeamBet
    {
        public int? TeamId { get; set; }

        public virtual ITeam BetTeam { get; set; }

        public TeamBet()
        {
            this.Type = BetType.Team;
        }
    }
}
