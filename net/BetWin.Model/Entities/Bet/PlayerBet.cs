﻿namespace BetWin.Model
{
    public interface IPlayerBet : IBet
    {
        int? PlayerId { get; set; }

        Player BetPlayer { get; set; }
    }

    public class PlayerBet : Bet, IPlayerBet
    {
        public int? PlayerId { get; set; }

        public virtual Player BetPlayer { get; set; }

        public PlayerBet()
        {
            this.Type = BetType.Player;
        }
    }
}
