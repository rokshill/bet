﻿namespace BetWin.Model
{
    public interface IMatchScoreBet : IBet
    {
        int? MatchScoreId { get; set; }

        MatchScore MatchScore { get; set; }
    }

    public sealed class MatchScoreBet : Bet, IMatchScoreBet
    {
        public int? MatchScoreId { get; set; }

        public MatchScore MatchScore { get; set; }

        public MatchScoreBet()
        {
            this.Type = BetType.MatchScore;
        }

        public MatchScoreBet(MatchScoreType scoreType, int scoreA, int scoreB){
            MatchScore = new MatchScore(scoreType, scoreA, scoreB);
        }
    }
}
