﻿namespace BetWin.Model
{
    public partial class Bet : BaseEntity, IBet
    {
        public int BetId { get; set; }

        public BetType Type { get; set; }

        public int UserId { get; set; }
        public int BetQuestionId { get; set; }
        public int? BetResultId { get; set; }

        public virtual User User { get; set; }
        public virtual BetQuestion BetQuestion { get; set; }
        public virtual BetResult BetResult { get; set; }
    }
}
