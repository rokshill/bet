﻿namespace BetWin.Model
{
    public interface IBet : IBaseEntity
    {
        int BetId { get; set; }

        BetType Type { get; set; }

        int UserId { get; set; }
        int BetQuestionId { get; set; }
        int? BetResultId { get; set; }

        User User { get; set; }
        BetQuestion BetQuestion { get; set; }
        BetResult BetResult { get; set; }
    }
}
