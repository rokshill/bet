﻿using System;

namespace BetWin.Model
{
    public class BaseEntity : IBaseEntity
    {
        public int CreatedById { get; set; } = 0;
        public DateTime CreatedOn { get; set; } = DateTime.UtcNow;
        public int? ModifiedById { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public virtual User CreatedBy { get; set; }
        public virtual User ModifiedBy { get; set; }
    }
}
