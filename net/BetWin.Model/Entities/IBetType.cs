﻿namespace BetWin.Model
{
    public interface IBetType
    {
        BetType Type { get; set; }
    }
}
