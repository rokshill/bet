﻿namespace BetWin.Model
{
    public partial class MatchScore
    {
        public MatchGeneralScoreType GetGeneralScoreType()
        {
            if (ScoreA < 0 || ScoreB < 0)
                return MatchGeneralScoreType.IncorrectScore;

            if (ScoreA > ScoreB)
                return MatchGeneralScoreType.HomeScore;
            else if (ScoreA < ScoreB)
                return MatchGeneralScoreType.AwayScore;
            else
                return MatchGeneralScoreType.DrawScore;
        }

        public bool EqualsInGeneral(MatchScore otherScore)
        {
            var thisGeneralScore = this.GetGeneralScoreType();
            var otherGeneralScore = otherScore.GetGeneralScoreType();

            if (thisGeneralScore == MatchGeneralScoreType.IncorrectScore ||
               otherGeneralScore == MatchGeneralScoreType.IncorrectScore)
                return false;

            return this.GetGeneralScoreType() == otherScore.GetGeneralScoreType();
        }

        public bool EqualsInExact(MatchScore otherScore)
        {
            var thisGeneralScore = this.GetGeneralScoreType();
            var otherGeneralScore = otherScore.GetGeneralScoreType();

            if (thisGeneralScore == MatchGeneralScoreType.IncorrectScore ||
               otherGeneralScore == MatchGeneralScoreType.IncorrectScore)
                return false;

            if (thisGeneralScore != otherGeneralScore)
                return false;

            var result = this.ScoreType.Equals(otherScore.ScoreType) &&
                         this.ScoreA.Equals(otherScore.ScoreA) &&
                         this.ScoreB.Equals(otherScore.ScoreB);

            return result;
        }
    }
}
