﻿namespace BetWin.Model
{
    public interface IMatchScore : IBaseEntity, IMatchScoreQuestionResultId, IBetType
    {
        MatchGeneralScoreType GetGeneralScoreType();
        bool EqualsInGeneral(MatchScore otherScore);
        bool EqualsInExact(MatchScore otherScore);
    }

    public partial class MatchScore : BaseEntity, IMatchScore
    {
        public MatchScore() { }
        public MatchScore(MatchScoreType type, int scoreA, int scoreB)
        {
            this.ScoreType = type;
            this.ScoreA = scoreA;
            this.ScoreB = scoreB;
        }

        public int MatchScoreId { get; set; }

        public BetType Type { get; set; } = BetType.MatchScore;
        public MatchScoreType ScoreType { get; set; }
        public int ScoreA { get; set; }
        public int ScoreB { get; set; }

        public int MatchId { get; set; }
        public int BetQuestionId { get; set; }
        public int? BetId { get; set; }

        public virtual Match Match { get; set; }
        public virtual IMatchScoreQuestion Question { get; set; }
        public virtual IMatchScoreBet Bet { get; set; }
    }
}
