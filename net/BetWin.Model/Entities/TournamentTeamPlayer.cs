﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetWin.Model
{
    public class TournamentTeamPlayer
    {
        public int Id { get; set; }

        public int? TournamentId { get; set; }
        public int? TeamId { get; set; }
        public int? PlayerId { get; set; }

        public virtual Tournament Tournament { get; set; }
        public virtual Team Team { get; set; }
        public virtual Player Player { get; set; }
    }
}
