﻿using System;
using System.Collections.Generic;

namespace BetWin.Model
{
    public class Tournament : BaseEntity
    {
        public int TournamentId { get; set; }

        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool IsActive { get; set; } = false;
        public TournamentType Type { get; set; }

        public virtual ICollection<Group> Groups { get; set; } = new HashSet<Group>();
        public virtual ICollection<TournamentTeamPlayer> TeamsPlayers { get; set; } = new HashSet<TournamentTeamPlayer>();
        public virtual ICollection<Match> Matches { get; set; } = new HashSet<Match>();
    }
}
