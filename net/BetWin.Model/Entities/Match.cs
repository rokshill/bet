﻿using System;
using System.Collections.Generic;

namespace BetWin.Model
{
    public class Match : BaseEntity
    {
        public int MatchId { get; set; }

        public DateTime DateTime { get; set; }
        public MatchType Type { get; set; }

        public int TeamAId { get; set; }
        public int TeamBId { get; set; }
        public int TournamentId { get; set; }
        public int? GroupId { get; set; }
        public int? FinalResultId { get; set; }
        public int? LocationId { get; set; }

        public virtual Team TeamA { get; set; }
        public virtual Team TeamB { get; set; }
        public virtual Tournament Tournament { get; set; }
        public virtual Group Group { get; set; }
        public virtual MatchScore FinalScore { get; set; }
        public virtual Location Location { get; set; }

        public virtual ICollection<MatchScore> Scores { get; set; } = new HashSet<MatchScore>();
        public virtual ICollection<MatchEvent> Events { get; set; } = new HashSet<MatchEvent>();
    }
}
