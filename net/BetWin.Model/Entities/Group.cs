﻿using System.Collections.Generic;

namespace BetWin.Model
{
    public class Group : BaseEntity
    {
        public int GroupId { get; set; }

        public string Name { get; set; }

        public int TournamentId { get; set; }

        public virtual Tournament Tournament { get; set; }

        public virtual ICollection<Team> Teams { get; set; } = new HashSet<Team>();
        public virtual ICollection<Match> Matches { get; set; } = new HashSet<Match>();
    }
}
