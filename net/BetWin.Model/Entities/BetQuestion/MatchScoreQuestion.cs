﻿namespace BetWin.Model
{
    public interface IMatchScoreQuestion : IBetQuestion, IMatchScoreQuestionResult
    {
    }

    public sealed class MatchScoreQuestion : BetQuestion, IMatchScoreQuestion
    {
        public int MatchScoreId { get; set; }

        public MatchScore MatchScore { get; set; }

        public MatchScoreQuestion()
        {
            this.Type = BetType.MatchScore;
        }

        public MatchScoreQuestion(MatchScoreType scoreType, int scoreA, int scoreB)
        {
            this.MatchScore = new MatchScore(scoreType, scoreA, scoreB);
        }
    }
}
