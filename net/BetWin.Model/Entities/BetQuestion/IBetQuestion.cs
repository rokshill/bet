﻿using System;
using System.Collections.Generic;

namespace BetWin.Model
{
    public interface IBetQuestion
    {
        int BetQuestionId { get; set; }

        string Code { get; set; }
        BetType Type { get; set; }
        string Question { get; set; }
        string Description { get; set; }
        DateTime BetOpenTime { get; set; }
        DateTime BetCloseTime { get; set; }
        DateTime? BetCalculateTime { get; set; }
        

        ICollection<Bet> Bets { get; set; }
    }
}
