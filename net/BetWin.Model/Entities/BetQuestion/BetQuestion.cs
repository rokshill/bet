﻿using System;
using System.Collections.Generic;

namespace BetWin.Model
{
    public class BetQuestion : BaseEntity, IBetQuestion
    {
        public int BetQuestionId { get; set; }

        public string Code { get; set; }
        public BetType Type { get; set; }
        public string Question { get; set; }
        public string Description { get; set; }
        public DateTime BetOpenTime { get; set; }
        public DateTime BetCloseTime { get; set; }
        public DateTime? BetCalculateTime { get; set; }

        public virtual ICollection<Bet> Bets { get; set; } = new HashSet<Bet>();
    }
}
