﻿namespace BetWin.Model
{
    public interface ITeamQuestion : IBetQuestion, ITeamQuestionResult
    {
    }

    public class TeamQuestion : BetQuestion, ITeamQuestion, ITeamQuestionResult
    {
        public TeamQuestion()
        {
            this.Type = BetType.Team;
        }

        public int TeamId { get; set; }

        public virtual Team Team { get; set; }
    }
}
