﻿namespace BetWin.Model
{
    public interface IPlayerQuestion : IBetQuestion, IPlayerQuestionResult
    {

    }

    public class PlayerQuestion : BetQuestion, IPlayerQuestion, IPlayerQuestionResult
    {
        public PlayerQuestion()
        {
            this.Type = BetType.Player;
        }

        public int PlayerId { get; set; }

        public virtual Player Player { get; set; }
    }
}