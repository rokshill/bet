﻿using System.Collections.Generic;

namespace BetWin.Model
{
    public interface IPlayer : IBaseEntity, IPlayerQuestionResult, IBetType
    {
    }

    public class Player : BaseEntity, IPlayer
    {
        public int PlayerId { get; set; }

        public BetType Type { get; set; } = BetType.Player;
        public string Name { get; set; }
        public int Number { get; set; }

        public virtual ICollection<TournamentTeamPlayer> TournamentsTeams { get; set; } = new HashSet<TournamentTeamPlayer>();
        public virtual ICollection<MatchEvent> Events { get; set; } = new HashSet<MatchEvent>();
        public virtual ICollection<PlayerBet> Bets { get; set; } = new HashSet<PlayerBet>(); 
    }
}
