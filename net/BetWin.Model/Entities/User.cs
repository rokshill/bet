﻿using System.Collections.Generic;

namespace BetWin.Model
{
    using System;

    public class User
    {
        public int UserId { get; set; }
        //public Guid Guid { get; set; }

        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }

        public virtual ICollection<Bet> Bets { get; set; } = new HashSet<Bet>();
    }
}
