﻿namespace BetWin.Model
{
    public interface ITeamBetResult : IBetResult
    {
        int? TeamId { get; set; }

        Team Team { get; set; }
    }

    public partial class TeamBetResult : BetResult, ITeamBetResult
    {
        public int? TeamId { get; set; }

        public virtual Team Team { get; set; }
    }
}
