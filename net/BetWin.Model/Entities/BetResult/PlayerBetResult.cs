﻿namespace BetWin.Model
{
    public interface IPlayerBetResult : IBetResult
    {
        int? PlayerId { get; set; }

        Player Player { get; set; }
    }

    public partial class PlayerBetResult : BetResult, IPlayerBetResult
    {
        public int? PlayerId { get; set; }

        public virtual Player Player { get; set; }
    }
}
