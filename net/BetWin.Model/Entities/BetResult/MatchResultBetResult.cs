﻿namespace BetWin.Model
{
    public interface IMatchScoreBetResult : IBetResult
    {
        int? MatchScoreId { get; set; }

        MatchScore MatchScore { get; set; }
    }

    public class MatchScoreBetResult : BetResult, IMatchScoreBetResult
    {
        public int? MatchScoreId { get; set; }

        public virtual MatchScore MatchScore { get; set; }

        public MatchScoreBetResult()
        {
            this.Type = BetType.MatchScore;
        }
    }
}
