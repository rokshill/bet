﻿namespace BetWin.Model
{
    public interface IBetResult : IBaseEntity
    {
        int BetResultId { get; set; }

        BetResultStatus Status { get; set; }
        string Message { get; set; }
        int Points { get; set; }
        BetType Type { get; set; }

        int BetId { get; set; }

        Bet Bet { get; set; }
    }
}
