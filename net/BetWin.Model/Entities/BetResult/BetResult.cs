﻿namespace BetWin.Model
{
    public class BetResult : BaseEntity, IBetResult
    {
        public int BetResultId { get; set; }

        public BetResultStatus Status { get; set; }
        public string Message { get; set; }
        public int Points { get; set; }
        public BetType Type { get; set; }

        public int BetId { get; set; }

        public virtual Bet Bet { get; set; }
    }
}
