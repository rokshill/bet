﻿namespace BetWin.Model
{
    public static class Limits
    {
        public static class Team
        {
            public static int NameMaxLength = 128;
            public static int ShortNameMaxLength = 16;
            public static int CrestUrlMaxLength = 512;
        }
    }
}
