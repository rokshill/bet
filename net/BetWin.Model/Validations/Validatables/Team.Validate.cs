﻿using BetWin.Common;
using Rokshill.Common.Extensions;
using System.Collections.Generic;
using Rokshill.Validations;

namespace BetWin.Model
{
    public partial class Team : IValidatable
    {
        public virtual ValidationResult Validate()
        {
            var faults = new List<string>();
            var v = DomainVocabulary.GetInstance();

            if (Name.IsNullOrEmpty() || ShortName.IsNullOrEmpty() || CrestUrl.IsNullOrEmpty())
                faults.Add(v.Fields_are_required(v.Fields.Name, v.Fields.ShortName, v.Fields.CrestUrl));

            if (Name.Length > Limits.Team.NameMaxLength)
                faults.Add(v.Text_field_value_is_too_long_params(v.Fields.Name));

            if (ShortName.Length > Limits.Team.ShortNameMaxLength)
                faults.Add(v.Text_field_value_is_too_long_params(v.Fields.ShortName));

            if (CrestUrl.Length > Limits.Team.CrestUrlMaxLength)
                faults.Add(v.Text_field_value_is_too_long_params(v.Fields.CrestUrl));

            var result = faults.ToValidationResult();
            return result; // Depending on your needs and the model you can cache this locally.
        }
    }
}
