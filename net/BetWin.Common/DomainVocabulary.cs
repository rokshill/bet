﻿using Rokshill.Tests.Extensions;
using System.Globalization;
using System.Linq;
using System.Resources;

namespace BetWin.Common
{
    public sealed class Fields
    {
        public string Name = DomainVocabularyResx.Name;
        public string ShortName = DomainVocabularyResx.Shortname;
        public string CrestUrl = DomainVocabularyResx.CrestUrl;
    }

    public sealed class DomainVocabulary
    {
        static readonly DomainVocabulary Instance = new DomainVocabulary();

        ResourceManager _resourceManager;

        private DomainVocabulary()
        {
            _resourceManager =
                new ResourceManager("BetWin.Common.DomainVocabularyResx",
                typeof(DomainVocabularyResx).Assembly);
        }

        public static DomainVocabulary GetInstance()
        {
            return Instance;
        }

        private string GetStringValue(string name)
        {
            var culture = CultureInfo.CurrentCulture ?? CultureInfo.CreateSpecificCulture("pl-PL");
            return _resourceManager.GetString(name, culture);
        }

        public Fields Fields { get; set; } = new Fields();

        public string Validation_failed = DomainVocabularyResx.Validation_failed;

        public string Validation_succeeded = DomainVocabularyResx.Validation_succeeded;

        public string Text_field_value_is_too_long = DomainVocabularyResx.Text_field_value_is_too_long;

        public string Text_field_value_is_too_long_params(string fieldName)
        {
            return DomainVocabularyResx.Text_field_value_is_too_long_p0.FormatWith(fieldName);
        }

        public string Fields_are_required(params string[] fields)
        {
            if (!fields.Any())
                return string.Empty;

            if (fields.Count() == 1)
                return DomainVocabularyResx.Field_is_requiered.FormatWith(fields.First());

            return DomainVocabularyResx.Fields_are_required.FormatWith(string.Join("','", fields));
        }

        // Require for unit tests
        public string Unit_test
        {
            get { return DomainVocabularyResx.Unit_test; }
        }
    }
}
