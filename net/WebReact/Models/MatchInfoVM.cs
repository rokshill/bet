﻿using System;
using BetWin.Model;

namespace WebReact.Models
{
    public class MatchInfoVm
    {
        public int MatchId { get; set; }
        public DateTime DateTime { get; set; }
        public MatchType Type { get; set; }
        public virtual Team TeamA { get; set; }
        public virtual Team TeamB { get; set; }
    }
}
