﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;

namespace WebReact.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IMapper Mapper = Code.AutoMapper.GetAutoMapper().Config.CreateMapper();
    }
}
