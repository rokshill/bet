﻿using System.Collections.Generic;
using AutoMapper;
using BetWin.Business.BusinessRepositories;
using BetWin.Model;
using Microsoft.AspNetCore.Mvc;
using WebReact.Models;

namespace WebReact.Controllers
{
    [Produces("application/json")]
    //[Route("api/Matches")]
    [Route("api/[controller]")]
    public class BetWinController : Controller
    {
        private readonly IBusinessRepository _businessRepo;
        
        private BetWinController()
        {
            _businessRepo = new BusinessRepository();
        }

        [HttpGet("[action]")]
        public IEnumerable<MatchInfoVm> Matches(int tournamentCode)
        {
            var matches = _businessRepo.GetAllMatchesByTournamentCode("WORLDCUP2018");
            var matchesVms = Mapper.Map<IEnumerable<Match>, IEnumerable<MatchInfoVm>>(matches);
            return matchesVms;
        }
    }
}