﻿using System;

namespace BetWin.WebApi.Models
{
    public class TournamentInfoVM
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndTime { get; set; }
        public bool IsActive { get; set; } = false;
    }
}