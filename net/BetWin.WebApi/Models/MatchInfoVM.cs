﻿using System;
using BetWin.Model;

namespace BetWin.WebApi.Models
{
    public class MatchInfoVM
    {
        public int MatchId { get; set; }
        public DateTime DateTime { get; set; }
        public MatchType Type { get; set; }
        public virtual Team TeamA { get; set; }
        public virtual Team TeamB { get; set; }
    }
}