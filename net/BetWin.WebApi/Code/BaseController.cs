﻿using System.Web.Http;
using AutoMapper;

namespace BetWin.WebApi.Code
{
    public class BaseController : ApiController
    {
        protected readonly IMapper Mapper = Code.AutoMapper.GetAutoMapper().Config.CreateMapper();
    }

}