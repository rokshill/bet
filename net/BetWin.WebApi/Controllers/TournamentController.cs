﻿using BetWin.Model;
using BetWin.WebApi.Models;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BetWin.Business.BusinessRepositories;
using BetWin.WebApi.Code;

namespace BetWin.WebApi.Controllers
{
    public class TournamentController : BaseController
    {
        private readonly ITournamentBusinessRepository _tournamentRepo;
        private readonly IBusinessRepository _businessRepo;

        private TournamentController()
        {
            _tournamentRepo = new TournamentBusinessRepository();
            _businessRepo = new BusinessRepository();
        }

        [Route("api/tournaments/")]
        public IEnumerable<TournamentInfoVM> Get()
        {
            var tournaments = _tournamentRepo.GetAllTournaments();
            var mapped = Mapper
                .Map<IEnumerable<Tournament>, IEnumerable<TournamentInfoVM>>(tournaments);
            return mapped;
        }

        [Route("api/tournaments/{code}/")]
        public HttpResponseMessage Get(string code)
        {
            var tournament =
                _tournamentRepo.GetTournamentByCode(code);

            if (tournament == null)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, 
                    "Tournament not found.");
            }

            var mapped = Mapper.Map<Tournament, TournamentInfoVM>(tournament);
            return Request.CreateResponse(mapped);
        }

     

    }
}
