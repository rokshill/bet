﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(BetWin.WebApi.Startup))]

namespace BetWin.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
