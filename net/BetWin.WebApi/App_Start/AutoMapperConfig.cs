﻿using AutoMapper;
using BetWin.Model;
using BetWin.WebApi.Models;

namespace BetWin.WebApi.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterMapping()
        {
            var automapper = Code.AutoMapper.GetAutoMapper();

            automapper.Config = new MapperConfiguration(cfg => { cfg.CreateMap<Tournament, TournamentInfoVM>(); });

            automapper.Config.AssertConfigurationIsValid();
        }
    }
}