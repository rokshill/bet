﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ImprovingMVCApplicationsWith10ExtensionPoints.Startup))]
namespace ImprovingMVCApplicationsWith10ExtensionPoints
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
