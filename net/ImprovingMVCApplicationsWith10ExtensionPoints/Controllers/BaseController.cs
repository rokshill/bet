﻿using System.Web.Mvc;
using AutoMapper;

namespace ImprovingMVCApplicationsWith10ExtensionPoints.Controllers
{
    public class BaseController : Controller
    {
        protected readonly IMapper Mapper = Code.AutoMapper.GetAutoMapper().Config.CreateMapper();
    }
}