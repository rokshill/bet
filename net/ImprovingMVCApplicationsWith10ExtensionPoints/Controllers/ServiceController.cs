﻿using System.Collections.Generic;
using System.Web.Mvc;
using BetWin.Business.BusinessRepositories;
using BetWin.Model;
using ImprovingMVCApplicationsWith10ExtensionPoints.Infrastructure;
using ImprovingMVCApplicationsWith10ExtensionPoints.Models;

namespace ImprovingMVCApplicationsWith10ExtensionPoints.Controllers
{
    public class ServiceController : BaseController
    {
        private readonly IBusinessRepository _businessRepo;

        public ServiceController()
        {
            _businessRepo = new BusinessRepository();
        }

        // GET: Service
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TournamentsXmlResult()
        {
            var tournaments = _businessRepo.GetAllTournaments();
            var tournamentsVms = Mapper.Map<IEnumerable<Tournament>, IEnumerable<TournamentInfoVM>>(tournaments);
            return new XmlResult(tournamentsVms);
        }

        public ActionResult TournamentsJsonResult()
        {
            var tournaments = _businessRepo.GetAllTournaments();
            var tournamentsVms = Mapper.Map<IEnumerable<Tournament>, IEnumerable<TournamentInfoVM>>(tournaments);
            return new Infrastructure.JsonResult(tournamentsVms);
        }

        public ActionResult TournamentsJsonFileResult()
        {
            var tournaments = _businessRepo.GetAllTournaments();
            var tournamentsVms = Mapper.Map<IEnumerable<Tournament>, IEnumerable<TournamentInfoVM>>(tournaments);
            return new JsonFileResult(tournamentsVms, "tournaments.json");
        }

        public ActionResult TournamentsCsvFileResult()
        {
            var tournaments = _businessRepo.GetAllTournaments();
            var tournamentsVms = Mapper.Map<IEnumerable<Tournament>, IEnumerable<TournamentInfoVM>>(tournaments);
            return new CsvFileResult(tournamentsVms, "tournaments.csv");
        }
    }
}