﻿using System.Web.Mvc;
using System.Xml.Serialization;

namespace ImprovingMVCApplicationsWith10ExtensionPoints.Infrastructure
{
    public class XmlResult : ActionResult
    {
        private readonly object _data;

        public XmlResult(object data)
        {
            _data = data;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var xmlSerializer = new XmlSerializer(_data.GetType());
            var response = context.HttpContext.Response;
            response.ContentType = "text/xml";
            xmlSerializer.Serialize(response.Output, _data);
        }
    }
}