﻿using System.Web.Mvc;
using Newtonsoft.Json;

namespace ImprovingMVCApplicationsWith10ExtensionPoints.Infrastructure
{
    public class JsonResult : ActionResult
    {
        private readonly object _data;

        public JsonResult(object data)
        {
            _data = data;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            var jsonSerializer = new JsonSerializer();
            var response = context.HttpContext.Response;
            response.ContentType = "text/json";
            jsonSerializer.Serialize(response.Output, _data);
        }
    }
}