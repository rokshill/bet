﻿using Newtonsoft.Json;
using System.Collections;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ImprovingMVCApplicationsWith10ExtensionPoints.Infrastructure
{
    public class JsonFileResult : FileResult
    {
        private IEnumerable _data;

        public JsonFileResult(IEnumerable data, string fileName) : base("text/json")
        {
            _data = data;
            FileDownloadName = fileName;
        }

        protected override void WriteFile(HttpResponseBase response)
        {
            var builder = new StringBuilder();
            var stringWriter = new StringWriter(builder);
            var jsonSerializer = new JsonSerializer();

            jsonSerializer.Serialize(stringWriter, _data);
            response.Write(builder);
        }

        public static string GetValue(object item, string propName)
        {
            return item.GetType().GetProperty(propName)?.GetValue(item, null)?.ToString() ?? string.Empty;
        }
    }
}
