﻿using BetWin.Model;
using System;

namespace ImprovingMVCApplicationsWith10ExtensionPoints.Models
{
    public class MatchInfoVM
    {
        public int MatchId { get; set; }
        public DateTime DateTime { get; set; }
        public MatchType Type { get; set; }
        public Team TeamA { get; set; }
        public Team TeamB { get; set; }
    }
}