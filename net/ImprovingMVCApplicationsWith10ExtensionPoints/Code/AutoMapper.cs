﻿using AutoMapper;

namespace ImprovingMVCApplicationsWith10ExtensionPoints.Code
{
    public class AutoMapper
    {
        private static readonly AutoMapper Instance = new AutoMapper();
        public MapperConfiguration Config { get; set; }

        private AutoMapper() { }

        public static AutoMapper GetAutoMapper()
        {
            return Instance;
        }
    }
}