﻿using AutoMapper;
using BetWin.Model;
using ImprovingMVCApplicationsWith10ExtensionPoints.Models;

namespace ImprovingMVCApplicationsWith10ExtensionPoints.App_Start
{
    public class AutoMapperConfig
    {
        public static void RegisterMapping()
        {
            var automapper = Code.AutoMapper.GetAutoMapper();

            automapper.Config = new MapperConfiguration(cfg => { cfg.CreateMap<Tournament, TournamentInfoVM>(); });
            automapper.Config = new MapperConfiguration(cfg => { cfg.CreateMap<Match, MatchInfoVM>(); });

            automapper.Config.AssertConfigurationIsValid();
        }
    }
}