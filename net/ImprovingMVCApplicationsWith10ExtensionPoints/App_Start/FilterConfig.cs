﻿using System.Web.Mvc;

namespace ImprovingMVCApplicationsWith10ExtensionPoints
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
