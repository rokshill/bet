﻿using BetWin.Model;
using Moq;

namespace BetWin.Tests.Mocks
{
    public class MatchScoreBetMock : Mock<IMatchScoreBet>
    {

        public MatchScoreBetMock SetScore(int scoreA, int scoreB, MatchScoreType type)
        {
            Setup(m =>
                m.MatchScore == new MatchScoreMock().SetScore(scoreA, scoreB, type).Object);

            return this;
        }
        
    }
}
