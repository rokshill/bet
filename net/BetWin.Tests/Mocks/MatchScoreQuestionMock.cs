﻿using BetWin.Model;
using Moq;

namespace BetWin.Tests.Mocks
{
    public class MatchScoreQuestionMock : Mock<MatchScoreQuestion>
    {
        public MatchScoreQuestionMock SetScore(MatchScoreType scoreType, int scoreA, int scoreB)
        {
            Setup(m =>
                m.MatchScore == new MatchScoreMock().SetScore(scoreA, scoreB, scoreType).Object);

            return this;
        }
    }
}
