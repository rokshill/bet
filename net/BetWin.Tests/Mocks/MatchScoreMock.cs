﻿using BetWin.Model;
using Moq;

namespace BetWin.Tests.Mocks
{
    public class MatchScoreMock : Mock<MatchScore>
    {
        public MatchScoreMock SetScore(int scoreA, int scoreB, MatchScoreType type)
        {
            Setup(m =>
                m.ScoreA == scoreA &&
                m.ScoreB == scoreB &&
                m.ScoreType == type);

            return this;
        }

    }
}
