﻿using BetWin.Model;
using Moq;

namespace BetWin.Tests.Mocks
{
    public class MatchScoreCalculatorConfigMock : Mock<MatchScoreCalculatorConfig>
    {
        public MatchScoreCalculatorConfigMock SetPoints(int generalScorePoints, int exactScorePoints)
        {
            Setup(m =>
                m.GeneralScorePoints == generalScorePoints &&
                m.ExactScorePoints == exactScorePoints);

            return this;
        }

        public MatchScoreCalculatorConfigMock SetGeneralScorePoints(int generalScorePoints)
        {
            Setup(m => m.GeneralScorePoints == generalScorePoints);

            return this;
        }

        public MatchScoreCalculatorConfigMock SetExactScorePoints(int exactScorePoints)
        {
            Setup(m => m.ExactScorePoints == exactScorePoints);

            return this;
        }
    }
}
