﻿using BetWin.Model;
using NUnit.Framework;
using Rokshill.Tests;

namespace BetWin.Tests.UnitTests.Entities.MatchScoreTests
{
    public class MatchScoreEqualsInGeneral : BaseTest
    {
        MatchScore _betScore;
        MatchScore _realScore;

        [TestCase(1, 0, 2, 1)]
        [TestCase(1, 1, 0, 0)]
        [TestCase(1, 2, 0, 3)]
        public void return_true_if_match_scores_are_equals_in_general(
            int betScoreA, int betScoreB, int realScoreA, int realScoerB)
        {
            bool excpect = true;
            _betScore = new MatchScore() { ScoreA = betScoreA, ScoreB = betScoreB };
            _realScore = new MatchScore() { ScoreA = realScoreA, ScoreB = realScoerB };

            Assert.AreEqual(excpect, _betScore.EqualsInGeneral(_realScore));
        }

        [TestCase(1, 0, 1, 2)]
        [TestCase(1, 1, 1, 2)]
        [TestCase(2, 1, 0, 0)]
        [TestCase(3, 0, 1, 2)]
        public void return_false_if_match_scores_are_not_equals_in_general(
            int betScoreA, int betScoreB, int realScoreA, int realScoerB)
        {
            bool expect = false;
            _betScore = new MatchScore() { ScoreA = betScoreA, ScoreB = betScoreB };
            _realScore = new MatchScore() { ScoreA = realScoreA, ScoreB = realScoerB };

            Assert.AreEqual(expect, _betScore.EqualsInGeneral(_realScore));
        }

        [TestCase(-1, 0, 1, 2)]
        [TestCase(1, -1, 1, 2)]
        public void return_false_if_passed_incorrect_match_score(
            int betScoreA, int betScoreB, int realScoreA, int realScoerB)
        {
            bool expect = false;
            _betScore = new MatchScore() { ScoreA = betScoreA, ScoreB = betScoreB };
            _realScore = new MatchScore() { ScoreA = realScoreA, ScoreB = realScoerB };

            Assert.AreEqual(expect, _betScore.EqualsInGeneral(_realScore));
        }
    }
}
