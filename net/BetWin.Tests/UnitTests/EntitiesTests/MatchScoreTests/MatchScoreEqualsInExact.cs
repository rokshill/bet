﻿using BetWin.Model;
using NUnit.Framework;
using Rokshill.Tests;

namespace BetWin.Tests.UnitTests.EntitiesTests.MatchScoreTests
{
    public class MatchScoreEqualsInExact : BaseTest
    {
        MatchScore _betScore;
        MatchScore _realScore;

        [TestCase(1, 0, 1, 0)]
        [TestCase(1, 2, 1, 2)]
        [TestCase(3, 3, 3, 3)]
        public void return_true_if_match_scores_are_equals_in_exact(
            int betScoreA, int betScoreB, int realScoreA, int realScoerB)
        {
            bool excpect = true;
            _betScore = new MatchScore() { ScoreA = betScoreA, ScoreB = betScoreB };
            _realScore = new MatchScore() { ScoreA = realScoreA, ScoreB = realScoerB };

            Assert.AreEqual(excpect, _betScore.EqualsInExact(_realScore));
        }

        [TestCase(1, 0, 2, 1)]
        [TestCase(1, 1, 2, 2)]
        [TestCase(2, 1, 1, 2)]
        [TestCase(1, 2, 1, 3)]
        public void return_false_if_match_scores_are_not_equals_in_exact(
            int betScoreA, int betScoreB, int realScoreA, int realScoerB)
        {
            bool expect = false;
            _betScore = new MatchScore() { ScoreA = betScoreA, ScoreB = betScoreB };
            _realScore = new MatchScore() { ScoreA = realScoreA, ScoreB = realScoerB };

            Assert.AreEqual(expect, _betScore.EqualsInExact(_realScore));
        }

        [TestCase(-1, 0, -1, 0)]
        [TestCase(1, -1, 1, 2)]
        public void return_false_if_passed_incorrect_match_score(
            int betScoreA, int betScoreB, int realScoreA, int realScoerB)
        {
            bool expect = false;
            _betScore = new MatchScore() { ScoreA = betScoreA, ScoreB = betScoreB };
            _realScore = new MatchScore() { ScoreA = realScoreA, ScoreB = realScoerB };

            Assert.AreEqual(expect, _betScore.EqualsInExact(_realScore));
        }
    }
}
