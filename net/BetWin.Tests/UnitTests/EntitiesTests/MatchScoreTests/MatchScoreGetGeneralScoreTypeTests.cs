﻿using BetWin.Model;
using NUnit.Framework;
using Rokshill.Tests;

namespace BetWin.Tests.UnitTests.Entities
{
    public class MatchScoreGetGeneralScoreTypeTests : BaseTest
    {
        MatchScore _matchScore;

        private MatchGeneralScoreType act()
        {
            return _matchScore.GetGeneralScoreType();
        }

        [TestCase(MatchGeneralScoreType.AwayScore, 0, 1)]
        [TestCase(MatchGeneralScoreType.HomeScore, 5, 3)]
        [TestCase(MatchGeneralScoreType.DrawScore, 0, 0)]
        [TestCase(MatchGeneralScoreType.DrawScore, 8, 8)]
        public void correct_general_score_types_test(MatchGeneralScoreType expectedType, int scoreA, int scoreB)
        {
            _matchScore = new MatchScore() { ScoreA = scoreA, ScoreB = scoreB };

            Assert.AreEqual(expectedType, act());
        }

        [TestCase(MatchGeneralScoreType.IncorrectScore, -1, 0)]
        [TestCase(MatchGeneralScoreType.IncorrectScore, 3, -2)]
        [TestCase(MatchGeneralScoreType.IncorrectScore, -2, -2)]
        public void incorrect_general_score_types_test(MatchGeneralScoreType expectedType, int scoreA, int scoreB)
        {
            _matchScore = new MatchScore() { ScoreA = scoreA, ScoreB = scoreB };

            Assert.AreEqual(expectedType, act());
        }

        [TestCase(MatchGeneralScoreType.HomeScore, 0, 1)]
        [TestCase(MatchGeneralScoreType.DrawScore, 2, 1)]
        [TestCase(MatchGeneralScoreType.AwayScore, 3, 0)]
        public void wrong_general_score_types_test(MatchGeneralScoreType expectedType, int scoreA, int scoreB)
        {
            _matchScore = new MatchScore() { ScoreA = scoreA, ScoreB = scoreB };

            Assert.AreNotEqual(expectedType, act());
        }
        
    }
}
