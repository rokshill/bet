﻿using BetWin.Business.Calculators;
using BetWin.Model;
using NUnit.Framework;
using Rokshill.Tests;
using System;

namespace BetWin.Tests.UnitTests.Calculators
{
    public class MatchScoreBetCalculatorTests : BaseTest
    {
        IMatchScoreBet _bet;
        MatchScoreCalculatorConfig _calculatorConfig;
        MatchScoreBetCalculator _calculator;
        IMatchScoreBetResult _betResult;
        MatchScoreQuestion _betQuestionResult;

        [SetUp]
        public void SetUp()
        {}

        [Test]
        public void throws_argument_null_exception_if_null_config_passed()
        {
            _calculatorConfig = null;

            Assert.Throws<ArgumentNullException>(
                () => new MatchScoreBetCalculator(_calculatorConfig));
        }

        [TestCase(1, MatchScoreType.RegularTime, 1, 2, MatchScoreType.RegularTime, 1, 3)]
        [TestCase(1, MatchScoreType.ExtraTime, 3, 0, MatchScoreType.RegularTime, 1, 0)]
        [TestCase(1, MatchScoreType.RegularTime, 0, 0, MatchScoreType.Penalties, 1, 1)]
        public void return_expected_number_of_points_for_hit_general_score(
            int expectedNumberOfPoints,
            MatchScoreType betScoreType, int betScoreA, int betScoreB,
            MatchScoreType finalScoreType, int finalScoreA, int finalScoreB)
        {
            _bet = new MatchScoreBet(betScoreType, betScoreA, betScoreB);
            _betQuestionResult = new MatchScoreQuestion(finalScoreType, finalScoreA, finalScoreB);
            _calculatorConfig = new MatchScoreCalculatorConfig(expectedNumberOfPoints, 0);
            _calculator = new MatchScoreBetCalculator(_calculatorConfig);
            
            _betResult = _calculator.Calculate(_bet, _betQuestionResult);

            Assert.AreEqual(expectedNumberOfPoints, _betResult.Points);
        }

        [TestCase(0, MatchScoreType.RegularTime, 1, 2, MatchScoreType.RegularTime, 2, 1)]
        [TestCase(0, MatchScoreType.ExtraTime, 3, 0, MatchScoreType.RegularTime, 0, 0)]
        [TestCase(0, MatchScoreType.RegularTime, 2, 1, MatchScoreType.Penalties, 4, 5)]
        public void return_expected_number_of_points_for_missed_score(
            int expectedNumberOfPoints,
            MatchScoreType betScoreType, int betScoreA, int betScoreB,
            MatchScoreType finalScoreType, int finalScoreA, int finalScoreB)
        {
            _bet = new MatchScoreBet(betScoreType, betScoreA, betScoreB);
            _betQuestionResult = new MatchScoreQuestion(finalScoreType, finalScoreA, finalScoreB);
            _calculatorConfig = new MatchScoreCalculatorConfig(expectedNumberOfPoints, 0);
            _calculator = new MatchScoreBetCalculator(_calculatorConfig);

            _betResult = _calculator.Calculate(_bet, _betQuestionResult);

            Assert.AreEqual(expectedNumberOfPoints, _betResult.Points);
        }

        [TestCase(3, MatchScoreType.RegularTime, 1, 2, MatchScoreType.RegularTime, 1, 2)]
        [TestCase(3, MatchScoreType.ExtraTime, 3, 2, MatchScoreType.ExtraTime, 3, 2)]
        [TestCase(3, MatchScoreType.Penalties, 5, 4, MatchScoreType.Penalties, 5, 4)]
        public void return_expected_number_of_points_for_hit_exact_score(
            int expectedNumberOfPoints,
            MatchScoreType betScoreType, int betScoreA, int betScoreB,
            MatchScoreType finalScoreType, int finalScoreA, int finalScoreB)
        {
            _bet = new MatchScoreBet(betScoreType, betScoreA, betScoreB);
            _betQuestionResult = new MatchScoreQuestion(finalScoreType, finalScoreA, finalScoreB);
            _calculatorConfig = new MatchScoreCalculatorConfig(0, expectedNumberOfPoints);
            _calculator = new MatchScoreBetCalculator(_calculatorConfig);

            _betResult = _calculator.Calculate(_bet, _betQuestionResult);

            Assert.AreEqual(expectedNumberOfPoints, _betResult.Points);
        }

    }
}
