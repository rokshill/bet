﻿using BetWin.Common;
using NUnit.Framework;
using Rokshill.Tests;
using System.Globalization;

namespace BetWin.Tests.UnitTests
{
    public class DomainVocabularyTests : BaseTest
    {
        string act()
        {
            return DomainVocabulary.GetInstance().Unit_test;
        }
        
        [Test]
        public void Should_Return_Polish_Text()
        {
            CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("pl-PL");
            CultureInfo.CurrentUICulture = CultureInfo.CreateSpecificCulture("pl-PL");

            Assert.AreEqual("Test jednostkowy", act());
        }

        [Test]
        public void Should_Return_English_Text()
        {
            CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
            CultureInfo.CurrentUICulture = CultureInfo.CreateSpecificCulture("en-US");

            Assert.AreEqual("Unit test", act());
        }
    }
}
