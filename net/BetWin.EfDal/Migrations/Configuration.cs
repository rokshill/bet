using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Text;

namespace BetWin.EfDAL.Migrations
{
    using BetWin.Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<BetWin.EfDAL.BetWinContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        
        
        protected override void Seed(BetWin.EfDAL.BetWinContext context)
        {
            SeedHelper.Seed(context);

            DataHelpers.SaveChanges(context);
        }
    }
}
