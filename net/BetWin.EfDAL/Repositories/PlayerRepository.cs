﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface IPlayerRepository : IGenericDataRepository<Player>
    {
    }

    public class PlayerRepository : GenericDataRepository<Player>, IPlayerRepository
    {
    }
}
