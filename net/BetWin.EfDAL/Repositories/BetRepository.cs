﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface IBetRepository : IGenericDataRepository<Bet>
    {
    }

    public class BetRepository : GenericDataRepository<Bet>, IBetRepository
    {
    }
}
