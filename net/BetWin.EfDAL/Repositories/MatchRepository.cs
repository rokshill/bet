﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface IMatchRepository : IGenericDataRepository<Match>
    {
    }

    public class MatchRepository : GenericDataRepository<Match>, IMatchRepository
    {
    }
}
