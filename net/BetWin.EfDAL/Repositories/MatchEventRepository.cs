﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface IMatchEventRepository : IGenericDataRepository<MatchEvent>
    {
    }

    public class MatchEventRepository : GenericDataRepository<MatchEvent>, IMatchEventRepository
    {
    }
}
