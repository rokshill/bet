﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface IBetResultRepository : IGenericDataRepository<BetResult>
    {
    }

    public class BetResultRepository : GenericDataRepository<BetResult>, IBetResultRepository
    {
    }
}
