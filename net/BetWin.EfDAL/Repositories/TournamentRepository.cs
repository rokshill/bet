﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface ITournamentRepository : IGenericDataRepository<Tournament>
    {
    }

    public class TournamentRepository : GenericDataRepository<Tournament>, ITournamentRepository
    {
    }
}
