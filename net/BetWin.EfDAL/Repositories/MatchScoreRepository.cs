﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface IMatchScoreRepository : IGenericDataRepository<MatchScore>
    {
    }

    public class MatchScoreRepository : GenericDataRepository<MatchScore>, IMatchScoreRepository
    {
    }
}
