﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface IUserRepository : IGenericDataRepository<User>
    {
    }

    public class UserRepository : GenericDataRepository<User>, IUserRepository
    {
    }
}
