﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface ILocationRepository : IGenericDataRepository<Location>
    {
    }

    public class LocationRepository : GenericDataRepository<Location>, ILocationRepository
    {
    }
}
