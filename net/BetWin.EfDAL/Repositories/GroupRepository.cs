﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface IGroupRepository : IGenericDataRepository<Group>
    {
    }

    public class GroupRepository : GenericDataRepository<Group>, IGroupRepository
    {
    }
}
