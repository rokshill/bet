﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface IBetQuestiontRepository : IGenericDataRepository<BetQuestion>
    {
    }

    public class BeQuestiontRepository : GenericDataRepository<BetQuestion>, IBetQuestiontRepository
    {
    }
}
