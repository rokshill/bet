﻿
using BetWin.Model;

namespace BetWin.EfDAL.Repositories
{
    public interface ITeamRepository : IGenericDataRepository<Team>
    {
    }

    public class TeamRepository : GenericDataRepository<Team>, ITeamRepository
    {
    }
}
