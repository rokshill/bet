﻿using System;
using BetWin.EfDAL.EntitiesConfiguration;
using BetWin.Model;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Linq;

namespace BetWin.EfDAL
{
    public class BetWinContext : DbContext
    {
        public BetWinContext()
            : base("wc2018")
        {
            Database.Log = sql => Debug.Write($"BetDbLog: {sql}");
        }


        public BetWinContext(string nameOrConnectionString = "wc2018")
            : base(nameOrConnectionString)
        {
            Database.Log = sql => Debug.Write($"BetDbLog: {sql}");
        }

        public static BetWinContext Create()
        {
            return new BetWinContext();
        }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("bet");

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new BetEntityConfiguration());
            modelBuilder.Configurations.Add(new BetQuestionEntityConfiguration());
            modelBuilder.Configurations.Add(new BetResultEntityConfiguration());
            modelBuilder.Configurations.Add(new GroupEntityConfiguration());
            modelBuilder.Configurations.Add(new LocationEntityConfiguration());
            modelBuilder.Configurations.Add(new MatchEntityConfiguration());
            modelBuilder.Configurations.Add(new MatchEventEntityConfiguration());
            modelBuilder.Configurations.Add(new MatchScoreEntityConfiguration());
            modelBuilder.Configurations.Add(new PlayerEntityConfiguration());
            modelBuilder.Configurations.Add(new TeamEntityConfiguration());
            modelBuilder.Configurations.Add(new TournamentEntityConfiguration());
            modelBuilder.Configurations.Add(new UserEntityConfiguration());
            modelBuilder.Configurations.Add(new TournamentTeamPlayerEntityConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            foreach (var change in this.ChangeTracker.Entries()
                .Where(e =>  e.Entity is IBaseEntity && (e.State==EntityState.Added || e.State == EntityState.Modified))
                .Select(e => e.Entity as IBaseEntity))
            {
                change.ModifiedOn = DateTime.UtcNow;
                if(change.CreatedOn == DateTime.MinValue)
                    change.CreatedOn = DateTime.UtcNow;
            }
            return base.SaveChanges();
        }


        public DbSet<Tournament> Tournaments { get; set; }

        public DbSet<Group> Groups { get; set; }

        public DbSet<Location> Locations { get; set; }
    
        public DbSet<Match> Matches { get; set; }

        public DbSet<MatchEvent> MatchEvents { get; set; }

        public DbSet<MatchScore> MatchResults { get; set; }

        public DbSet<Player> Players { get; set; }

        public DbSet<Team> Teams { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<Bet> Bets { get; set; }

        public DbSet<BetResult> BetResults { get; set; }

        public DbSet<TournamentTeamPlayer> TournamentTeamPlayers { get; set; }
        
    }
}
