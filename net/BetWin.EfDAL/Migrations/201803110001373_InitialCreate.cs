namespace BetWin.EfDAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "bet.BetResult",
                c => new
                    {
                        BetResultId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Message = c.String(),
                        Points = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        BetId = c.Int(nullable: false),
                        CreatedById = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedById = c.Int(),
                        ModifiedOn = c.DateTime(),
                        MatchScoreId = c.Int(),
                        PlayerId = c.Int(),
                        TeamId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.BetResultId)
                .ForeignKey("bet.Bet", t => t.BetResultId)
                .ForeignKey("bet.User", t => t.CreatedById)
                .ForeignKey("bet.User", t => t.ModifiedById)
                .ForeignKey("bet.MatchScore", t => t.MatchScoreId)
                .ForeignKey("bet.Player", t => t.PlayerId)
                .ForeignKey("bet.Team", t => t.TeamId)
                .Index(t => t.BetResultId)
                .Index(t => t.CreatedById)
                .Index(t => t.ModifiedById)
                .Index(t => t.MatchScoreId)
                .Index(t => t.PlayerId)
                .Index(t => t.TeamId);
            
            CreateTable(
                "bet.Bet",
                c => new
                    {
                        BetId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        BetQuestionId = c.Int(nullable: false),
                        BetResultId = c.Int(),
                        CreatedById = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedById = c.Int(),
                        ModifiedOn = c.DateTime(),
                        PlayerId = c.Int(),
                        TeamId = c.Int(),
                        MatchScoreId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.BetId)
                .ForeignKey("bet.BetQuestion", t => t.BetQuestionId, cascadeDelete: true)
                .ForeignKey("bet.Player", t => t.PlayerId)
                .ForeignKey("bet.Team", t => t.TeamId)
                .ForeignKey("bet.User", t => t.CreatedById)
                .ForeignKey("bet.User", t => t.ModifiedById)
                .ForeignKey("bet.User", t => t.UserId)
                .ForeignKey("bet.MatchScore", t => t.MatchScoreId)
                .Index(t => t.UserId)
                .Index(t => t.BetQuestionId)
                .Index(t => t.CreatedById)
                .Index(t => t.ModifiedById)
                .Index(t => t.PlayerId)
                .Index(t => t.TeamId)
                .Index(t => t.MatchScoreId);
            
            CreateTable(
                "bet.BetQuestion",
                c => new
                    {
                        BetQuestionId = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 512),
                        Type = c.Int(nullable: false),
                        Question = c.String(nullable: false, maxLength: 512),
                        Description = c.String(maxLength: 1024),
                        BetOpenTime = c.DateTime(nullable: false),
                        BetCloseTime = c.DateTime(nullable: false),
                        BetCalculateTime = c.DateTime(),
                        CreatedById = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedById = c.Int(),
                        ModifiedOn = c.DateTime(),
                        MatchScoreId = c.Int(),
                        PlayerId = c.Int(),
                        TeamId = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.BetQuestionId)
                .ForeignKey("bet.User", t => t.CreatedById)
                .ForeignKey("bet.User", t => t.ModifiedById)
                .ForeignKey("bet.MatchScore", t => t.MatchScoreId, cascadeDelete: true)
                .ForeignKey("bet.Player", t => t.PlayerId, cascadeDelete: true)
                .ForeignKey("bet.Team", t => t.TeamId, cascadeDelete: true)
                .Index(t => t.CreatedById)
                .Index(t => t.ModifiedById)
                .Index(t => t.MatchScoreId)
                .Index(t => t.PlayerId)
                .Index(t => t.TeamId);
            
            CreateTable(
                "bet.User",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 32),
                        Email = c.String(nullable: false, maxLength: 256),
                        Password = c.String(maxLength: 512),
                        Role = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserId);
            
            CreateTable(
                "bet.MatchScore",
                c => new
                    {
                        MatchScoreId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        ScoreType = c.Int(nullable: false),
                        ScoreA = c.Int(nullable: false),
                        ScoreB = c.Int(nullable: false),
                        MatchId = c.Int(nullable: false),
                        BetQuestionId = c.Int(nullable: false),
                        BetId = c.Int(),
                        CreatedById = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedById = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.MatchScoreId)
                .ForeignKey("bet.User", t => t.CreatedById)
                .ForeignKey("bet.Match", t => t.MatchId, cascadeDelete: true)
                .ForeignKey("bet.User", t => t.ModifiedById)
                .Index(t => t.MatchId)
                .Index(t => t.CreatedById)
                .Index(t => t.ModifiedById);
            
            CreateTable(
                "bet.Match",
                c => new
                    {
                        MatchId = c.Int(nullable: false, identity: true),
                        DateTime = c.DateTime(nullable: false),
                        Type = c.Int(nullable: false),
                        TeamAId = c.Int(nullable: false),
                        TeamBId = c.Int(nullable: false),
                        TournamentId = c.Int(nullable: false),
                        GroupId = c.Int(),
                        FinalResultId = c.Int(),
                        LocationId = c.Int(),
                        CreatedById = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedById = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.MatchId)
                .ForeignKey("bet.User", t => t.CreatedById)
                .ForeignKey("bet.MatchScore", t => t.FinalResultId)
                .ForeignKey("bet.Group", t => t.GroupId)
                .ForeignKey("bet.Location", t => t.LocationId)
                .ForeignKey("bet.User", t => t.ModifiedById)
                .ForeignKey("bet.Team", t => t.TeamAId)
                .ForeignKey("bet.Team", t => t.TeamBId)
                .ForeignKey("bet.Tournament", t => t.TournamentId, cascadeDelete: true)
                .Index(t => t.TeamAId)
                .Index(t => t.TeamBId)
                .Index(t => t.TournamentId)
                .Index(t => t.GroupId)
                .Index(t => t.FinalResultId)
                .Index(t => t.LocationId)
                .Index(t => t.CreatedById)
                .Index(t => t.ModifiedById);
            
            CreateTable(
                "bet.MatchEvent",
                c => new
                    {
                        MatchEventId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Minute = c.Int(nullable: false),
                        PlayerId = c.Int(),
                        TeamId = c.Int(nullable: false),
                        MatchId = c.Int(nullable: false),
                        CreatedById = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedById = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.MatchEventId)
                .ForeignKey("bet.User", t => t.CreatedById)
                .ForeignKey("bet.User", t => t.ModifiedById)
                .ForeignKey("bet.Player", t => t.PlayerId)
                .ForeignKey("bet.Team", t => t.TeamId)
                .ForeignKey("bet.Match", t => t.MatchId, cascadeDelete: true)
                .Index(t => t.PlayerId)
                .Index(t => t.TeamId)
                .Index(t => t.MatchId)
                .Index(t => t.CreatedById)
                .Index(t => t.ModifiedById);
            
            CreateTable(
                "bet.Player",
                c => new
                    {
                        PlayerId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Name = c.String(maxLength: 128),
                        Number = c.Int(),
                        CreatedById = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedById = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.PlayerId)
                .ForeignKey("bet.User", t => t.CreatedById)
                .ForeignKey("bet.User", t => t.ModifiedById)
                .Index(t => t.CreatedById)
                .Index(t => t.ModifiedById);
            
            CreateTable(
                "bet.TournamentTeamPlayer",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TournamentId = c.Int(nullable: false),
                        TeamId = c.Int(nullable: false),
                        PlayerId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("bet.Tournament", t => t.TournamentId, cascadeDelete: true)
                .ForeignKey("bet.Team", t => t.TeamId, cascadeDelete: true)
                .ForeignKey("bet.Player", t => t.PlayerId)
                .Index(t => t.TournamentId)
                .Index(t => t.TeamId)
                .Index(t => t.PlayerId);
            
            CreateTable(
                "bet.Team",
                c => new
                    {
                        TeamId = c.Int(nullable: false, identity: true),
                        Type = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 127),
                        ShortName = c.String(nullable: false, maxLength: 15),
                        CrestUrl = c.String(maxLength: 511),
                        GroupId = c.Int(),
                        CreatedById = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedById = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.TeamId)
                .ForeignKey("bet.User", t => t.CreatedById)
                .ForeignKey("bet.Group", t => t.GroupId)
                .ForeignKey("bet.User", t => t.ModifiedById)
                .Index(t => t.GroupId)
                .Index(t => t.CreatedById)
                .Index(t => t.ModifiedById);
            
            CreateTable(
                "bet.Group",
                c => new
                    {
                        GroupId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 64),
                        TournamentId = c.Int(nullable: false),
                        CreatedById = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedById = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.GroupId)
                .ForeignKey("bet.User", t => t.CreatedById)
                .ForeignKey("bet.User", t => t.ModifiedById)
                .ForeignKey("bet.Tournament", t => t.TournamentId, cascadeDelete: true)
                .Index(t => t.TournamentId)
                .Index(t => t.CreatedById)
                .Index(t => t.ModifiedById);
            
            CreateTable(
                "bet.Tournament",
                c => new
                    {
                        TournamentId = c.Int(nullable: false, identity: true),
                        Code = c.String(nullable: false, maxLength: 32),
                        Name = c.String(nullable: false, maxLength: 128),
                        StartTime = c.DateTime(),
                        EndTime = c.DateTime(),
                        IsActive = c.Boolean(nullable: false),
                        Type = c.Int(nullable: false),
                        CreatedById = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedById = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.TournamentId)
                .ForeignKey("bet.User", t => t.CreatedById)
                .ForeignKey("bet.User", t => t.ModifiedById)
                .Index(t => t.CreatedById)
                .Index(t => t.ModifiedById);
            
            CreateTable(
                "bet.Location",
                c => new
                    {
                        LocationId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        Latitude = c.Double(nullable: false),
                        Longitude = c.Double(nullable: false),
                        Capacity = c.Int(nullable: false),
                        CreatedById = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        ModifiedById = c.Int(),
                        ModifiedOn = c.DateTime(),
                    })
                .PrimaryKey(t => t.LocationId)
                .ForeignKey("bet.User", t => t.CreatedById)
                .ForeignKey("bet.User", t => t.ModifiedById)
                .Index(t => t.CreatedById)
                .Index(t => t.ModifiedById);
            
        }
        
        public override void Down()
        {
            DropForeignKey("bet.BetResult", "TeamId", "bet.Team");
            DropForeignKey("bet.BetResult", "PlayerId", "bet.Player");
            DropForeignKey("bet.BetResult", "MatchScoreId", "bet.MatchScore");
            DropForeignKey("bet.BetResult", "ModifiedById", "bet.User");
            DropForeignKey("bet.BetResult", "CreatedById", "bet.User");
            DropForeignKey("bet.BetResult", "BetResultId", "bet.Bet");
            DropForeignKey("bet.Bet", "MatchScoreId", "bet.MatchScore");
            DropForeignKey("bet.Bet", "UserId", "bet.User");
            DropForeignKey("bet.Bet", "ModifiedById", "bet.User");
            DropForeignKey("bet.Bet", "CreatedById", "bet.User");
            DropForeignKey("bet.BetQuestion", "TeamId", "bet.Team");
            DropForeignKey("bet.BetQuestion", "PlayerId", "bet.Player");
            DropForeignKey("bet.BetQuestion", "MatchScoreId", "bet.MatchScore");
            DropForeignKey("bet.MatchScore", "ModifiedById", "bet.User");
            DropForeignKey("bet.Match", "TournamentId", "bet.Tournament");
            DropForeignKey("bet.Match", "TeamBId", "bet.Team");
            DropForeignKey("bet.Match", "TeamAId", "bet.Team");
            DropForeignKey("bet.MatchScore", "MatchId", "bet.Match");
            DropForeignKey("bet.Match", "ModifiedById", "bet.User");
            DropForeignKey("bet.Location", "ModifiedById", "bet.User");
            DropForeignKey("bet.Match", "LocationId", "bet.Location");
            DropForeignKey("bet.Location", "CreatedById", "bet.User");
            DropForeignKey("bet.Match", "GroupId", "bet.Group");
            DropForeignKey("bet.Match", "FinalResultId", "bet.MatchScore");
            DropForeignKey("bet.MatchEvent", "MatchId", "bet.Match");
            DropForeignKey("bet.MatchEvent", "TeamId", "bet.Team");
            DropForeignKey("bet.MatchEvent", "PlayerId", "bet.Player");
            DropForeignKey("bet.TournamentTeamPlayer", "PlayerId", "bet.Player");
            DropForeignKey("bet.TournamentTeamPlayer", "TeamId", "bet.Team");
            DropForeignKey("bet.Team", "ModifiedById", "bet.User");
            DropForeignKey("bet.Group", "TournamentId", "bet.Tournament");
            DropForeignKey("bet.TournamentTeamPlayer", "TournamentId", "bet.Tournament");
            DropForeignKey("bet.Tournament", "ModifiedById", "bet.User");
            DropForeignKey("bet.Tournament", "CreatedById", "bet.User");
            DropForeignKey("bet.Team", "GroupId", "bet.Group");
            DropForeignKey("bet.Group", "ModifiedById", "bet.User");
            DropForeignKey("bet.Group", "CreatedById", "bet.User");
            DropForeignKey("bet.Team", "CreatedById", "bet.User");
            DropForeignKey("bet.Bet", "TeamId", "bet.Team");
            DropForeignKey("bet.Player", "ModifiedById", "bet.User");
            DropForeignKey("bet.Player", "CreatedById", "bet.User");
            DropForeignKey("bet.Bet", "PlayerId", "bet.Player");
            DropForeignKey("bet.MatchEvent", "ModifiedById", "bet.User");
            DropForeignKey("bet.MatchEvent", "CreatedById", "bet.User");
            DropForeignKey("bet.Match", "CreatedById", "bet.User");
            DropForeignKey("bet.MatchScore", "CreatedById", "bet.User");
            DropForeignKey("bet.BetQuestion", "ModifiedById", "bet.User");
            DropForeignKey("bet.BetQuestion", "CreatedById", "bet.User");
            DropForeignKey("bet.Bet", "BetQuestionId", "bet.BetQuestion");
            DropIndex("bet.Location", new[] { "ModifiedById" });
            DropIndex("bet.Location", new[] { "CreatedById" });
            DropIndex("bet.Tournament", new[] { "ModifiedById" });
            DropIndex("bet.Tournament", new[] { "CreatedById" });
            DropIndex("bet.Group", new[] { "ModifiedById" });
            DropIndex("bet.Group", new[] { "CreatedById" });
            DropIndex("bet.Group", new[] { "TournamentId" });
            DropIndex("bet.Team", new[] { "ModifiedById" });
            DropIndex("bet.Team", new[] { "CreatedById" });
            DropIndex("bet.Team", new[] { "GroupId" });
            DropIndex("bet.TournamentTeamPlayer", new[] { "PlayerId" });
            DropIndex("bet.TournamentTeamPlayer", new[] { "TeamId" });
            DropIndex("bet.TournamentTeamPlayer", new[] { "TournamentId" });
            DropIndex("bet.Player", new[] { "ModifiedById" });
            DropIndex("bet.Player", new[] { "CreatedById" });
            DropIndex("bet.MatchEvent", new[] { "ModifiedById" });
            DropIndex("bet.MatchEvent", new[] { "CreatedById" });
            DropIndex("bet.MatchEvent", new[] { "MatchId" });
            DropIndex("bet.MatchEvent", new[] { "TeamId" });
            DropIndex("bet.MatchEvent", new[] { "PlayerId" });
            DropIndex("bet.Match", new[] { "ModifiedById" });
            DropIndex("bet.Match", new[] { "CreatedById" });
            DropIndex("bet.Match", new[] { "LocationId" });
            DropIndex("bet.Match", new[] { "FinalResultId" });
            DropIndex("bet.Match", new[] { "GroupId" });
            DropIndex("bet.Match", new[] { "TournamentId" });
            DropIndex("bet.Match", new[] { "TeamBId" });
            DropIndex("bet.Match", new[] { "TeamAId" });
            DropIndex("bet.MatchScore", new[] { "ModifiedById" });
            DropIndex("bet.MatchScore", new[] { "CreatedById" });
            DropIndex("bet.MatchScore", new[] { "MatchId" });
            DropIndex("bet.BetQuestion", new[] { "TeamId" });
            DropIndex("bet.BetQuestion", new[] { "PlayerId" });
            DropIndex("bet.BetQuestion", new[] { "MatchScoreId" });
            DropIndex("bet.BetQuestion", new[] { "ModifiedById" });
            DropIndex("bet.BetQuestion", new[] { "CreatedById" });
            DropIndex("bet.Bet", new[] { "MatchScoreId" });
            DropIndex("bet.Bet", new[] { "TeamId" });
            DropIndex("bet.Bet", new[] { "PlayerId" });
            DropIndex("bet.Bet", new[] { "ModifiedById" });
            DropIndex("bet.Bet", new[] { "CreatedById" });
            DropIndex("bet.Bet", new[] { "BetQuestionId" });
            DropIndex("bet.Bet", new[] { "UserId" });
            DropIndex("bet.BetResult", new[] { "TeamId" });
            DropIndex("bet.BetResult", new[] { "PlayerId" });
            DropIndex("bet.BetResult", new[] { "MatchScoreId" });
            DropIndex("bet.BetResult", new[] { "ModifiedById" });
            DropIndex("bet.BetResult", new[] { "CreatedById" });
            DropIndex("bet.BetResult", new[] { "BetResultId" });
            DropTable("bet.Location");
            DropTable("bet.Tournament");
            DropTable("bet.Group");
            DropTable("bet.Team");
            DropTable("bet.TournamentTeamPlayer");
            DropTable("bet.Player");
            DropTable("bet.MatchEvent");
            DropTable("bet.Match");
            DropTable("bet.MatchScore");
            DropTable("bet.User");
            DropTable("bet.BetQuestion");
            DropTable("bet.Bet");
            DropTable("bet.BetResult");
        }
    }
}
