﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BetWin.Model;

namespace BetWin.EfDAL.Migrations
{
    public class SeedHelper
    {
        private const int SystemId = 0;
        private const int AdminId = 1;
        private const int RokshillId = 2;

        private const int WorldCupId = 1;

        private const int GroupAId = 1;
        private const int GroupBId = 2;
        private const int GroupCId = 3;
        private const int GroupDId = 4;
        private const int GroupEId = 5;
        private const int GroupFId = 6;
        private const int GroupGId = 7;
        private const int GroupHId = 8;


        private const int EgyptId = 1;
        private const int RussiaId = 2;
        private const int SaudiId = 3;
        private const int UruguayId = 4;

        private const int IranId = 5;
        private const int MoroccoId = 6;
        private const int PortugalId = 7;
        private const int SpainId = 8;

        private const int AustraliaId = 9;
        private const int DenmarkId = 10;
        private const int FranceId = 11;
        private const int PeruId = 12;

        private const int ArgentinaId = 13;
        private const int CroatiaId = 14;
        private const int IcelandId = 15;
        private const int NigeriaId = 16;
        
        private const int BrazilId = 17;
        private const int CostaRicaId = 18;
        private const int SerbiaId = 19;
        private const int SwitzerlandId = 20;

        private const int GermanyId = 21;
        private const int SouthKoreaId = 22;
        private const int MexicoId = 23;
        private const int SwedenId = 24;

        private const int BelgiumId = 25;
        private const int EnglandId = 26;
        private const int PanamaId = 27;
        private const int TunisiaId = 28;

        private const int ColombiaId = 29;
        private const int JapanId = 30;
        private const int PolandId = 31;
        private const int SenegalId = 32;

        private static User CreateUser(int id, string name, string email)
        {
            return new User
            {
                UserId = id,
                Role = UserRole.Admin,
                UserName = name,
                Email = email
            };
        }

        private static Tournament CreateTournament(int id, string code, string name, DateTime startTime, bool isActive)
        {
            return new Tournament()
            {
                TournamentId = id,
                Code = code,
                Name = name,
                IsActive = isActive,
                StartTime = startTime.ToUniversalTime(),
                CreatedById = SystemId,
                CreatedOn = DateTime.UtcNow
            };
        }

        private static Group CreateGroup(int id, int tournamentId, string name)
        {
            return new Group()
            {
                GroupId = id,
                TournamentId = tournamentId,
                Name = name,
                CreatedById = SystemId,
                CreatedOn = DateTime.UtcNow
            };
        }

        private static Team CreateTeam(int id, Tournament tournament, int? groupId, string name, string shortName, string crestUrl)
        {
            return new Team()
            {
                TeamId = id,
                TournamentsPlayers = new List<TournamentTeamPlayer>()
                {
                    CreateTournamentTeamPlayer(tournament.TournamentId, id, null)
                },
                GroupId = groupId,
                Name = name,
                ShortName = shortName,
                CrestUrl = crestUrl,
                CreatedById = SystemId,
                CreatedOn = DateTime.UtcNow
            };
        }

        private static TournamentTeamPlayer CreateTournamentTeamPlayer(int? tournamentId, int? teamId, int? playerId)
        {
            return new TournamentTeamPlayer()
            {
                TournamentId = tournamentId,
                TeamId = teamId,
                PlayerId = playerId
            };
        }

        private static Match CreateMatch(int id, int tournamentId, int groupId, DateTime dateTime, int teamAId, int teamBId,
            MatchType matchType = MatchType.Group)
        {
            return new Match()
            {
                MatchId = id,
                TournamentId = tournamentId,
                GroupId = groupId,
                DateTime = dateTime.ToUniversalTime(),
                TeamAId = teamAId,
                TeamBId = teamBId,
                Type = matchType,
                CreatedById = SystemId,
                CreatedOn = DateTime.UtcNow
            };
        }

        public static void Seed(BetWinContext context)
        {
            context.Users.AddOrUpdate(
                CreateUser(SystemId, "system", "rokshill@gmail.com"),
                CreateUser(AdminId, "admin", "rokshill@gmail.com")//,
                //CreateUser(RokshillId, "rokshill", "rokshill@gmail.com")
            );

            var worldCup = CreateTournament(WorldCupId, "WORLDCUP2018", "World Cup 2018", new DateTime(2018, 06, 14), true);
            context.Tournaments.AddOrUpdate(worldCup);

            int id = 3;
            context.Groups.AddOrUpdate(
                CreateGroup(GroupAId, WorldCupId, "Grupa A"),
                CreateGroup(GroupBId, WorldCupId, "Grupa B"),
                CreateGroup(GroupCId, WorldCupId, "Grupa C"),
                CreateGroup(GroupDId, WorldCupId, "Grupa D"),
                CreateGroup(GroupEId, WorldCupId, "Grupa E"),
                CreateGroup(GroupFId, WorldCupId, "Grupa F"),
                CreateGroup(GroupGId, WorldCupId, "Grupa G"),
                CreateGroup(GroupHId, WorldCupId, "Grupa H")
            );

            context.Teams.AddOrUpdate(
                // Grupa A
                CreateTeam(EgyptId, worldCup, GroupAId, "Egipt", "EGY", "wc2018/flags/egypt.png"),
                CreateTeam(RussiaId, worldCup, GroupAId, "Rosja", "RUS", "wc2018/flags/egypt.png"),
                CreateTeam(SaudiId, worldCup, GroupAId, "Arabia Saudyjska", "SAU", "wc2018/flags/saudi_arabia.png"),
                CreateTeam(UruguayId, worldCup, GroupAId, "Urugwaj", "URY", "wc2018/flags/uruguay.png"),
                // Grupa B
                CreateTeam(IranId, worldCup, GroupBId, "Iran", "IRN", "wc2018/flags/iran.png"),
                CreateTeam(MoroccoId, worldCup, GroupBId, "Maroko", "MAR", "wc2018/flags/morocco.png"),
                CreateTeam(PortugalId, worldCup, GroupBId, "Portugalia", "PRT", "wc2018/flags/portugal.png"),
                CreateTeam(SpainId, worldCup, GroupBId, "Hiszpania", "ESP", "wc2018/flags/spain.png"),
                // Grupa C
                CreateTeam(AustraliaId, worldCup, GroupCId, "Australia", "AUS", "wc2018/flags/australia.png"),
                CreateTeam(DenmarkId, worldCup, GroupCId, "Dania", "DEN", "wc2018/flags/denmark.png"),
                CreateTeam(FranceId, worldCup, GroupCId, "Francja", "FRA", "wc2018/flags/france.png"),
                CreateTeam(PeruId, worldCup, GroupCId, "Peru", "PER", "wc2018/flags/peru.png"),
                // Grupa D
                CreateTeam(ArgentinaId, worldCup, GroupDId, "Argentyna", "ARG", "wc2018/flags/argentina.png"),
                CreateTeam(CroatiaId, worldCup, GroupDId, "Chorwacja", "CRO", "wc2018/flags/croatia.png"),
                CreateTeam(IcelandId, worldCup, GroupDId, "Islandia", "ICE", "wc2018/flags/iceland.png"),
                CreateTeam(NigeriaId, worldCup, GroupDId, "Nigeria", "NIG", "wc2018/flags/nigeria.png"),
                // Grupa E
                CreateTeam(BrazilId, worldCup, GroupEId, "Brazylia", "BRA", "wc2018/flags/brazil.png"),
                CreateTeam(CostaRicaId, worldCup, GroupEId, "Kostaryka", "COS", "wc2018/flags/costa_rica.png"),
                CreateTeam(SerbiaId, worldCup, GroupEId, "Serbia", "SER", "wc2018/flags/serbia.png"),
                CreateTeam(SwitzerlandId, worldCup, GroupEId, "Szwajcaria", "SWI", "wc2018/flags/switzerland.png"),
                // Grupa F
                CreateTeam(GermanyId, worldCup, GroupFId, "Niemcy", "GER", "wc2018/flags/germany.png"),
                CreateTeam(SouthKoreaId, worldCup, GroupFId, "Korea Południowa", "KOR", "wc2018/flags/south_korea.png"),
                CreateTeam(MexicoId, worldCup, GroupFId, "Meksyk", "MEX", "wc2018/flags/mexico.png"),
                CreateTeam(SwedenId, worldCup, GroupFId, "Szwecja", "SWE", "wc2018/flags/sweden.png"),
                // Grupa G
                CreateTeam(BelgiumId, worldCup, GroupGId, "Belgia", "BEL", "wc2018/flags/belgium.png"),
                CreateTeam(EnglandId, worldCup, GroupGId, "Anglia", "ENG", "wc2018/flags/england.png"),
                CreateTeam(PanamaId, worldCup, GroupGId, "Panama", "PAN", "wc2018/flags/panama.png"),
                CreateTeam(TunisiaId, worldCup, GroupGId, "Tunezja", "TUN", "wc2018/flags/tunisia.png"),
                // Grupa H
                CreateTeam(ColombiaId, worldCup, GroupHId, "Kolumbia", "COL", "wc2018/flags/colombia.png"),
                CreateTeam(JapanId, worldCup, GroupHId, "Japonia", "JAP", "wc2018/flags/japan.png"),
                CreateTeam(PolandId, worldCup, GroupHId, "Polska", "POL", "wc2018/flags/poland.png"),
                CreateTeam(SenegalId, worldCup, GroupHId, "Senegal", "SEN", "wc2018/flags/senegal.png")
            );
            id = 1;
            context.Matches.AddOrUpdate(
                CreateMatch(id++, WorldCupId, GroupAId, new DateTime(2018, 6, 14, 17, 0, 0), RussiaId, SaudiId),
                CreateMatch(id++, WorldCupId, GroupAId, new DateTime(2018, 6, 15, 14, 0, 0), EgyptId, UruguayId),
                CreateMatch(id++, WorldCupId, GroupAId, new DateTime(2018, 6, 15, 17, 0, 0), MoroccoId, IranId),
                CreateMatch(id++, WorldCupId, GroupAId, new DateTime(2018, 6, 15, 20, 0, 0), PortugalId, SpainId));
            
        }
    }
}
