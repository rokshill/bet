﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class MatchScoreEntityConfiguration : BaseEntityConfiguration<MatchScore>
    {
        public MatchScoreEntityConfiguration()
        {
            this.HasKey(m => m.MatchScoreId);

            this.Property(m => m.ScoreType)
                .IsRequired();

            this.Property(m => m.ScoreA)
                .IsRequired();

            this.Property(m => m.ScoreB)
                .IsRequired();

            this.HasRequired(m => m.Match)
                .WithMany(m => m.Scores)
                .HasForeignKey(m => m.MatchId);
        }
    }
}
