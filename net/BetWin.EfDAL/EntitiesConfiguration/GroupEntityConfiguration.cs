﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class GroupEntityConfiguration : BaseEntityConfiguration<Group>
    {
        public GroupEntityConfiguration()
        {
            this.HasKey(g => g.GroupId);

            this.Property(g => g.Name)
                .IsRequired()
                .HasMaxLength(64);

            this.HasRequired(g => g.Tournament)
                .WithMany(t => t.Groups)
                .HasForeignKey(g => g.TournamentId);

            this.HasMany(g => g.Teams)
                .WithOptional(t => t.Group)
                .HasForeignKey(g => g.GroupId);
        }
    }
}
