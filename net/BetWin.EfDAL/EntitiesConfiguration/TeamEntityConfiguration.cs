﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class TeamEntityConfiguration : BaseEntityConfiguration<Team>
    {
        public TeamEntityConfiguration()
        {
            this.HasKey(t => t.TeamId);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(Limits.Team.NameMaxLength);

            this.Property(t => t.ShortName)
                .IsRequired()
                .HasMaxLength(Limits.Team.ShortNameMaxLength);

            this.Property(t => t.CrestUrl)
                .IsOptional()
                .HasMaxLength(Limits.Team.CrestUrlMaxLength);

            this.HasMany(g => g.TournamentsPlayers)
                .WithRequired(t => t.Team)
                .HasForeignKey(p => p.TeamId);

            this.HasMany(t => t.HomeMatches)
                .WithRequired(m => m.TeamA)
                .HasForeignKey(m => m.TeamAId)
                .WillCascadeOnDelete(false);

            this.HasMany(t => t.AwayMatches)
                .WithRequired(m => m.TeamB)
                .HasForeignKey(m => m.TeamBId)
                .WillCascadeOnDelete(false);
            
            this.HasMany(p => p.Events)
                .WithRequired(e => e.Team)
                .HasForeignKey(e => e.TeamId)
                .WillCascadeOnDelete(false);
        }
    }
}
