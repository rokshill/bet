﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class BetQuestionEntityConfiguration : BaseEntityConfiguration<BetQuestion>
    {
        public BetQuestionEntityConfiguration()
        {
            this.HasKey(e => e.BetQuestionId);

            this.Property(e => e.Code)
                .IsRequired()
                .HasMaxLength(512);

            this.Property(e => e.Type)
                .IsRequired();

            this.Property(e => e.Question)
                .IsRequired()
                .HasMaxLength(512);

            this.Property(e => e.Description)
                .IsOptional()
                .HasMaxLength(1024);
        }
    }
}
