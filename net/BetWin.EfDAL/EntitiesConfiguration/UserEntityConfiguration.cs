﻿using BetWin.Model;
using System.Data.Entity.ModelConfiguration;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class UserEntityConfiguration : EntityTypeConfiguration<User>
    {
        public UserEntityConfiguration()
        {
            this.HasKey(u => u.UserId);

            this.Property(u => u.UserName)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(u => u.Email)
                .IsRequired()
                .HasMaxLength(256);

            this.Property(u => u.Password)
                .HasMaxLength(512);

            this.Property(u => u.Role)
                .IsRequired();
        }
    }
}
