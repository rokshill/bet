﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class LocationEntityConfiguration : BaseEntityConfiguration<Location>
    {
        public LocationEntityConfiguration()
        {
            this.HasKey(l => l.LocationId);

            this.Property(l => l.Name)
                .IsRequired()
                .HasMaxLength(256);
        }
    }
}
