﻿using System.Data.Entity.ModelConfiguration;
using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    class TournamentTeamPlayerEntityConfiguration : EntityTypeConfiguration<TournamentTeamPlayer>
    {
        public TournamentTeamPlayerEntityConfiguration()
        {
            this.HasKey(pk => pk.Id);

            this.Property(u => u.TournamentId)
                .IsRequired();
        }
    }
}
