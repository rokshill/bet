﻿using BetWin.Model;
using System.Data.Entity.ModelConfiguration;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class BaseEntityConfiguration<TEntity> : EntityTypeConfiguration<TEntity> where TEntity : BaseEntity
    {
        public BaseEntityConfiguration()
        {
            this.HasRequired(e => e.CreatedBy)
                .WithMany()
                .HasForeignKey(e => e.CreatedById)
                .WillCascadeOnDelete(false);

            this.Property(e => e.CreatedOn).IsRequired();

            this.HasOptional(e => e.ModifiedBy)
                .WithMany()
                .HasForeignKey(e => e.ModifiedById)
                .WillCascadeOnDelete(false);

            this.Property(e => e.ModifiedOn).IsOptional();
        }
    }
}
