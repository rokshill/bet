﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class MatchEntityConfiguration : BaseEntityConfiguration<Match>
    {
        public MatchEntityConfiguration()
        {
            this.HasKey(m => m.MatchId);

            this.Property(m => m.DateTime)
                .IsRequired();

            this.Property(m => m.Type)
                .IsRequired();

            this.HasRequired(m => m.TeamA)
                .WithMany(t => t.HomeMatches)
                .HasForeignKey(m => m.TeamAId);

            this.HasRequired(m => m.TeamB)
                .WithMany(t => t.AwayMatches)
                .HasForeignKey(m => m.TeamBId);

            this.HasOptional(m => m.Group)
                .WithMany(g => g.Matches)
                .HasForeignKey(m => m.GroupId);

            this.HasRequired(g => g.Tournament)
                .WithMany(t => t.Matches)
                .HasForeignKey(g => g.TournamentId);

            this.HasMany(m => m.Scores)
                .WithRequired(r => r.Match)
                .HasForeignKey(r => r.MatchId);

            this.HasMany(p => p.Events)
                .WithRequired(e => e.Match)
                .HasForeignKey(e => e.MatchId);

            this.HasOptional(p => p.FinalScore)
                .WithMany()
                .HasForeignKey(e => e.FinalResultId);
        }
    }
}
