﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class PlayerBetEntityConfiguration : BaseEntityConfiguration<PlayerBet>
    {
        public PlayerBetEntityConfiguration()
        {
            this.HasRequired(g => g.BetPlayer)
                .WithMany(p => p.Bets)
                .HasForeignKey(g => g.PlayerId);               
        }
    }
}
