﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class PlayerEntityConfiguration : BaseEntityConfiguration<Player>
    {
        public PlayerEntityConfiguration()
        {
            this.HasKey(p => p.PlayerId);

            this.Property(p => p.Name)
                .IsOptional()
                .HasMaxLength(128);

            this.Property(p => p.Number)
                .IsOptional();

            this.HasMany(g => g.TournamentsTeams)
                .WithOptional(t => t.Player)
                .HasForeignKey(p => p.PlayerId);

            this.HasMany(p => p.Events)
                .WithOptional(e => e.Player)
                .HasForeignKey(e => e.PlayerId);
        }
    }
}
