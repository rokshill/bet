﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class MatchEventEntityConfiguration : BaseEntityConfiguration<MatchEvent>
    {
        public MatchEventEntityConfiguration()
        {
            this.HasKey(m => m.MatchEventId);

            this.Property(m => m.Type)
                .IsRequired();

            this.Property(m => m.Minute)
                .IsRequired();

            this.HasOptional(m => m.Player)
                .WithMany(p => p.Events)
                .HasForeignKey(m => m.PlayerId);

            this.HasRequired(m => m.Team)
                .WithMany(t => t.Events)
                .HasForeignKey(m => m.TeamId);

            this.HasRequired(m => m.Match)
                .WithMany(t => t.Events)
                .HasForeignKey(m => m.MatchId);
        }
    }
}
