﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class BetEntityConfiguration : BaseEntityConfiguration<Bet>
    {
        public BetEntityConfiguration()
        {
            this.HasKey(b => b.BetId);

            this.HasOptional(b => b.BetResult)
                .WithRequired(r => r.Bet);    
            
            this.HasRequired(b => b.User)
                .WithMany(u => u.Bets)
                .HasForeignKey(b => b.UserId)
                .WillCascadeOnDelete(false);
        }
    }
}
