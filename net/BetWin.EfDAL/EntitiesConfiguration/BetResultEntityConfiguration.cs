﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class BetResultEntityConfiguration : BaseEntityConfiguration<BetResult>
    {
        public BetResultEntityConfiguration()
        {
            this.HasKey(b => b.BetResultId);

            this.Property(b => b.Points)
                .IsRequired();

            this.HasRequired(b => b.Bet)
                .WithOptional(b => b.BetResult);
        }
    }
}