﻿using BetWin.Model;

namespace BetWin.EfDAL.EntitiesConfiguration
{
    public class TournamentEntityConfiguration : BaseEntityConfiguration<Tournament>
    {
        public TournamentEntityConfiguration()
        {
            this.HasKey(t => t.TournamentId);

            this.Property(t => t.Code)
                .IsRequired()
                .HasMaxLength(32);

            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(128);

            this.Property(t => t.StartTime)
                .IsOptional();

            this.Property(t => t.EndTime)
                .IsOptional();

            this.Property(t => t.IsActive)
                .IsRequired();

            this.Property(t => t.Type)
                .IsRequired();

            this.HasMany(t => t.Groups)
                .WithRequired(g => g.Tournament)
                .HasForeignKey(g => g.TournamentId);

            this.HasMany(t => t.TeamsPlayers)
                .WithRequired(t => t.Tournament)
                .HasForeignKey(t => t.TournamentId);
            
            this.HasMany(t => t.Matches)
                .WithRequired(m => m.Tournament)
                .HasForeignKey(m => m.TournamentId);

        }
    }
}
